library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use IEEE.math_real.all;

library work;

entity pipe5 is
    generic (
            l : boolean := false
        );
    port (
            lr       : in  std_logic;
            la       : out std_logic;
            rr       : out std_logic;
            ra       : in  std_logic;
            dataA_in  : in  std_logic_vector(63 downto 0);
            dataB_in  : in  std_logic_vector(63 downto 0);
            dataA_out : out std_logic_vector(63 downto 0);
            dataB_out : out std_logic_vector(63 downto 0);
            reset    : in  std_logic
        );
end entity pipe5;

architecture sync of pipe5 is

    component sc is
        generic (
            init_full : boolean := false
            );
        port (
            lr    : in  std_logic;
            la    : out std_logic;
            rr    : out std_logic;
            ra    : in  std_logic;
            reset : in  std_logic;
            clk   : out std_logic
            );
    end component sc;

    component mem is
        generic (
            la : boolean := false
            );
        port (
            D  : in  std_logic;
            RN : in  std_logic;
            CK : in  std_logic;
            Q  : out std_logic
            );
    end component mem;

    component delay_chain
        generic (
            n : natural
            );
        port (
            D : in  std_logic;
            Q : out std_logic
            );
        end component;
	
    signal reqs, reqs_del : std_logic_vector(5 downto 0);
    signal acks : std_logic_vector(5 downto 0);
    signal clks : std_logic_vector(5 downto 0);
    type data_vector is array(4 downto 0) of std_logic_vector(63 downto 0);
    signal dAi, dBi, dAo, dBo : data_vector;

begin
    
    -- controller
    stages: for i in 0 to 4 generate
        sc_i : sc
            generic map (
                init_full => false)
            port map (
                lr => reqs_del(i),
                la => acks(i),
                rr => reqs(i+1),
                ra => acks(i+1),
		clk => clks(i),
		reset => reset);
    end generate stages;

    reqs_del <= reqs;
    reqs(0) <= lr;
    acks(5) <= ra;
    la <= acks(0);
    rr <= reqs(5);

    -- datapath
    dp: for i in 0 to 4 generate
        reg: for y in 0 to 63 generate
            mem_A : mem
                generic map (
                    la => l)
                port map (
                    D  => dAi(i)(y),
                    Q  => dAo(i)(y),
                    CK => clks(i),
		    RN => reset);
            mem_B : mem
                generic map (
                    la => l)
                port map (
                    D  => dBi(i)(y),
                    Q  => dBo(i)(y),
                    CK => clks(i),
		    RN => reset);
        end generate reg;
    end generate dp;

    -- some dummy operations
    dAi(0) <= dataA_in;
    dBi(0) <= dataB_in;
    dAi(1) <= dAo(0) + dBo(0);
    dBi(1) <= dBo(0) + 1234567890;
    dAi(2) <= dAo(1)(31 downto 0) * dBo(1)(31 downto 0);
    dBi(2) <= dAo(1)(63 downto 32) * dBo(1)(63 downto 32) + 555555555;
    dAi(3) <= dAo(2)(31 downto 0) * dBo(2)(63 downto 32);
    dBi(3) <= dAo(2)(63 downto 32) * dBo(2)(31 downto 0) + 987654321;
    dAi(4) <= dAo(3) - dBo(3);
    dBi(4) <= (dAo(3) - dBo(3)) xor dBo(3);
    dataA_out <= dAo(4);
    dataB_out <= dBo(4);
	
end architecture;
