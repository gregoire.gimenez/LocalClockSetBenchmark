library ieee;
use ieee.std_logic_1164.all;

library work;

entity mem is

  generic (
    la : boolean := false);

  port (
    D  : in  std_logic;
    RN : in  std_logic;
    CK : in  std_logic;
    Q  : out std_logic);

end mem;

architecture saed of mem is

component LARX1_RVT is
    port (
        D : in std_logic;
        CLK : in std_logic;
        RSTB : in std_logic;
        Q : out std_logic;
        QN : out std_logic
        );
end component;

component DFFARX1_RVT is
    port (
        D : in std_logic;
        CLK : in std_logic;
        RSTB : in std_logic;
        Q : out std_logic
        );
end component;

begin  -- saed

    latch: if la generate
        mem : LARX1_RVT
            port map (
                D => D,
                CLK => CK,
                RSTB => RN,
                Q => Q
                );
    end generate latch;
 
    dff: if not la generate
        mem : DFFARX1_RVT
            port map (
                D => D,
                CLK => CK,
                RSTB => RN,
                Q => Q
                );
    end generate dff;
  
end saed;
