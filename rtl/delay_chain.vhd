library ieee;
use ieee.std_logic_1164.all;

entity delay_chain is
  
  generic (
    n : natural := 10);                 -- length of the chain

  port (
    D : in  std_logic;
    Q : out std_logic);

end delay_chain;

architecture tal of delay_chain is

  component delay
    port (
      D : in  std_logic;
      Q : out std_logic);
  end component;

  signal internals : std_logic_vector(n downto 0);
  
begin  -- tal

  internals(0) <= D;
  Q <= internals(n);

  chain: for i in 0 to n-1 generate
    inv_i: delay
      port map (
        D => internals(i),
        Q => internals(i+1));
  end generate chain;

end tal;
