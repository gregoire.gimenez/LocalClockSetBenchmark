library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use IEEE.math_real.all;

library work;

entity pipe is
    generic (
            l : boolean := false;
            n : natural := 10
        );
    port (
            lr       : in  std_logic;
            la       : out std_logic;
            rr       : out std_logic;
            ra       : in  std_logic;
            dA_in    : in  std_logic_vector(63 downto 0);
            dB_in    : in  std_logic_vector(63 downto 0);
            dA_out   : out std_logic_vector(63 downto 0);
            dB_out   : out std_logic_vector(63 downto 0);
            reset    : in  std_logic
        );
end entity pipe;

architecture netlist of pipe is

    component pipe5 is
        generic (
                l : boolean := false
            );
        port (
                lr       : in  std_logic;
                la       : out std_logic;
                rr       : out std_logic;
                ra       : in  std_logic;
                dataA_in  : in  std_logic_vector(63 downto 0);
                dataB_in  : in  std_logic_vector(63 downto 0);
                dataA_out : out std_logic_vector(63 downto 0);
                dataB_out : out std_logic_vector(63 downto 0);
                reset    : in  std_logic
            );
    end component pipe5;
	
    signal reqs : std_logic_vector(n downto 0);
    signal acks : std_logic_vector(n downto 0);
    type data_vector is array(n downto 0) of std_logic_vector(63 downto 0);
    signal dAi, dBi : data_vector;

begin

    
    -- controller
    stages: for i in 0 to n-1 generate
        p5_i : pipe5
            generic map (
                l => l)
            port map (
                lr => reqs(i),
                la => acks(i),
                rr => reqs(i+1),
                ra => acks(i+1),
                dataA_in  => dAi(i),
                dataB_in  => dBi(i),
                dataA_out => dAi(i+1),
                dataB_out => dBi(i+1),
		reset => reset);
    end generate stages;

    reqs(0) <= lr;
    acks(n) <= ra;
    la <= acks(0);
    rr <= reqs(n);
    dAi(0) <= dA_in;
    dBi(0) <= dB_in;
    dA_out <= dAi(n);
    dB_out <= dBi(n);
	
end architecture;
