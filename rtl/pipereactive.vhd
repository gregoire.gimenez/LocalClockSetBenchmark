library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use IEEE.math_real.all;

library work;

entity pipe is
    generic (
            l : boolean := false;
            n : natural := 10
        );
    port (
            lr       : in  std_logic;
            la       : out std_logic;
            rr       : out std_logic;
            ra       : in  std_logic;
            dA_in    : in  std_logic_vector(63 downto 0);
            dB_in    : in  std_logic_vector(63 downto 0);
            dA_out   : out std_logic_vector(63 downto 0);
            dB_out   : out std_logic_vector(63 downto 0);
            reset    : in  std_logic
        );
end entity pipe;

architecture netlist of pipe is

    component NAND2X1_RVT is
        port (
            A1 : in std_logic;
            A2 : in std_logic;
            Y  : out std_logic);
    end component;
    component NBUFFX2_RVT is
        port (
            A : in std_logic;
            Y : out std_logic);
    end component;
    component pipe5 is
        generic (
                l : boolean := false
            );
        port (
                lr       : in  std_logic;
                la       : out std_logic;
                rr       : out std_logic;
                ra       : in  std_logic;
                dataA_in  : in  std_logic_vector(63 downto 0);
                dataB_in  : in  std_logic_vector(63 downto 0);
                dataA_out : out std_logic_vector(63 downto 0);
                dataB_out : out std_logic_vector(63 downto 0);
                reset    : in  std_logic
            );
    end component pipe5;
	
    signal reqs : std_logic_vector(n downto 0);
    signal acks : std_logic_vector(n downto 0);
    type data_vector is array(n downto 0) of std_logic_vector(63 downto 0);
    signal dAi, dBi : data_vector;
    signal osc0, osc1, osc2, osc3, osc4, osc5, osc_out : std_logic;

begin

    
    -- controller
    stages: for i in 0 to n-1 generate
        p5_i : pipe5
            generic map (
                l => l)
            port map (
                lr => reqs(i),
                la => acks(i),
                rr => reqs(i+1),
                ra => acks(i+1),
                dataA_in  => dAi(i),
                dataB_in  => dBi(i),
                dataA_out => dAi(i+1),
                dataB_out => dBi(i+1),
		reset => reset);
    end generate stages;

    reqs(0) <= osc_out;
    acks(n) <= ra;
    la <= acks(0);
    rr <= reqs(n);
    dAi(0) <= dA_in;
    dBi(0) <= dB_in;
    dA_out <= dAi(n);
    dB_out <= dBi(n);

    gate_R: NAND2X1_RVT
        port map (
            A1   => osc5,
            A2   => reset,
            Y    => osc_out);
    osc_0: NBUFFX2_RVT
        port map (
            A   => osc_out,
            Y   => osc0);
    osc_1: NBUFFX2_RVT
        port map (
            A   => osc0,
            Y   => osc1);
    osc_2: NBUFFX2_RVT
        port map (
            A   => osc1,
            Y   => osc2);
    osc_3: NBUFFX2_RVT
        port map (
            A   => osc2,
            Y   => osc3);
    osc_4: NBUFFX2_RVT
        port map (
            A   => osc3,
            Y   => osc4);
    osc_5: NBUFFX2_RVT
        port map (
            A   => osc4,
            Y   => osc5);
	
end architecture;
