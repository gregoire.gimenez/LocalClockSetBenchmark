library ieee;
use ieee.std_logic_1164.all;

library work;

entity sc is
    generic (
        init_full : boolean := false
        );
    port (
        lr : in std_logic;
        la : out std_logic;
        rr : out std_logic;
        ra : in std_logic;
        clk : out std_logic;
        reset : in std_logic
        );
end entity sc;

architecture simple of sc is

    component OR3X1_RVT is
        port (
            A1 : in std_logic;
            A2 : in std_logic;
            A3 : in std_logic;
            Y  : out std_logic);
    end component;
    component OR2X1_RVT is
        port (
            A1 : in std_logic;
            A2 : in std_logic;
            Y  : out std_logic);
    end component;
    component AND2X1_RVT is
        port (
            A1 : in std_logic;
            A2 : in std_logic;
            Y  : out std_logic);
    end component;
    component XOR2X1_RVT is
        port (
            A1 : in std_logic;
            A2 : in std_logic;
            Y  : out std_logic);
    end component;
    component INVX1_RVT is
        port (
            A : in std_logic;
            Y : out std_logic);
    end component;
    component NBUFFX2_RVT is
        port (
            A : in std_logic;
            Y : out std_logic);
    end component;

    signal anda, andb, andc, or_out, or_rst, rai, del1, del2, del3, pulse, reseti : std_logic;        

begin  -- simple

    la  <= or_rst;
    rr  <= or_rst;
    clk <= pulse;

    delay_pulse1: NBUFFX2_RVT
        port map (
            A    => or_rst,
            Y    => del1);
    delay_pulse2: NBUFFX2_RVT
        port map (
            A    => del1,
            Y    => del2);
    delay_pulse3: NBUFFX2_RVT
        port map (
            A    => del2,
            Y    => del3);
    xor_pulse: XOR2X1_RVT
        port map (
            A1   => or_rst,
            A2   => del3,
            Y    => pulse);
    inv_I: INVX1_RVT
        port map (
            A    => ra,
            Y    => rai);
    or_Z: OR3X1_RVT
        port map (
            A1   => anda,
            A2   => andc,
            A3   => andb,
            Y    => or_out);
    and_A: AND2X1_RVT
        port map (
            A1   => or_rst,
            A2   => lr,
            Y    => anda);
    and_C: AND2X1_RVT
        port map (
            A1   => lr,
            A2   => rai,
            Y    => andc);
    and_B: AND2X1_RVT
        port map (
            A1   => rai,
            A2   => or_rst,
            Y    => andb);
    
    WITH_RST: if init_full generate
        inv_rst: INVX1_RVT
            port map (
                A    => reset,
                Y    => reseti);
        and_R: OR2X1_RVT
            port map (
                A1   => or_out,
                A2   => reseti,
                Y    => or_rst);
    end generate WITH_RST;

    WOUT_RST: if not init_full generate
        and_R: AND2X1_RVT
            port map (
                A1   => or_out,
                A2   => reset,
                Y    => or_rst);
    end generate WOUT_RST;

end architecture simple;
