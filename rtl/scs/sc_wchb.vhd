library ieee;
use ieee.std_logic_1164.all;

library work;

entity sc is
    generic (
        init_full : boolean := false
        );
    port (
        lr : in std_logic;
        la : out std_logic;
        rr : out std_logic;
        ra : in std_logic;
        clk : out std_logic;
        reset : in std_logic
        );
end entity sc;

architecture wchb of sc is

    component OR3X1_RVT is
        port (
            A1 : in std_logic;
            A2 : in std_logic;
            A3 : in std_logic;
            Y  : out std_logic);
    end component;
    component AND2X1_RVT is
        port (
            A1 : in std_logic;
            A2 : in std_logic;
            Y  : out std_logic);
    end component;
    component INVX1_RVT is
        port (
            A : in std_logic;
            Y : out std_logic);
    end component;

    signal anda, andb, andc, or_out, or_rst, rai : std_logic;        

begin  -- wchb

    la  <= or_rst;
    rr  <= or_rst;
    clk <= or_rst;
    
    WITH_RST: if init_full generate
        or_Z: OR3X1_RVT
            port map (
                A1   => anda,
                A2   => andc,
                A3   => andb,
                Y    => or_out);
        and_A: AND2X1_RVT
            port map (
                A1   => or_rst,
                A2   => lr,
                Y    => anda);
        and_C: AND2X1_RVT
            port map (
                A1   => lr,
                A2   => rai,
                Y    => andc);
        and_B: AND2X1_RVT
            port map (
                A1   => rai,
                A2   => or_rst,
                Y    => andb);
        and_R: AND2X1_RVT
            port map (
                A1   => or_out,
                A2   => reset,
                Y    => or_rst);
        inv_I: INVX1_RVT
            port map (
                A    => ra,
                Y    => rai);
    end generate WITH_RST;

    WOUT_RST: if not init_full generate
        or_Z: OR3X1_RVT
            port map (
                A1   => anda,
                A2   => andc,
                A3   => andb,
                Y    => or_out);
        and_A: AND2X1_RVT
            port map (
                A1   => or_rst,
                A2   => lr,
                Y    => anda);
        and_C: AND2X1_RVT
            port map (
                A1   => lr,
                A2   => rai,
                Y    => andc);
        and_B: AND2X1_RVT
            port map (
                A1   => rai,
                A2   => or_rst,
                Y    => andb);
        and_R: AND2X1_RVT
            port map (
                A1   => or_out,
                A2   => reset,
                Y    => or_rst);
        inv_I: INVX1_RVT
            port map (
                A    => ra,
                Y    => rai);
    end generate WOUT_RST;

end architecture wchb;
