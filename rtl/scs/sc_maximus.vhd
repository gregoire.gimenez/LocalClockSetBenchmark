library ieee;
use ieee.std_logic_1164.all;

library work;

entity sc is
    generic (
        init_full : boolean := false
        );
    port (
        lr : in std_logic;
        la : out std_logic;
        rr : out std_logic;
        ra : in std_logic;
        clk : out std_logic;
        reset : in std_logic
        );
end entity sc;

architecture maximus of sc is

    component OR3X1_RVT is
        port (
            A1 : in std_logic;
            A2 : in std_logic;
            A3 : in std_logic;
            Y  : out std_logic);
    end component;
    component OR2X1_RVT is
        port (
            A1 : in std_logic;
            A2 : in std_logic;
            Y  : out std_logic);
    end component;
    component AND2X1_RVT is
        port (
            A1 : in std_logic;
            A2 : in std_logic;
            Y  : out std_logic);
    end component;
    component INVX1_RVT is
        port (
            A : in std_logic;
            Y : out std_logic);
    end component;

    signal rr_tmp, la_tmp, laps, reseti, lai, rri : std_logic;
    signal cl_anda, cl_andb, cl_andc, cl_or_out : std_logic;        
    signal cr_anda, cr_andb, cr_andc, cr_or_out : std_logic;        
    signal cs_anda, cs_andb, cs_andc, cs_or_out : std_logic;        
    signal cl_and_in, cl_or_in : std_logic;        
    signal cr_and_in, cr_or_in : std_logic;        
    signal cs_and_in, cs_or_in : std_logic;        
    signal s : std_logic;  -- internal state

begin  -- maximus

    inv_rst : INVX1_RVT
        port map (
            A => reset,
            Y => reseti);
    inv_la : INVX1_RVT
        port map (
            A => la_tmp,
            Y => lai);
    inv_rri : INVX1_RVT
        port map (
            A => rri,
            Y => rr_tmp);

-- Cl
    cl_or_I: OR2X1_RVT
        port map (
            A1   => ra,
            A2   => lr,
            Y    => cl_or_in);
    cl_and_I: AND2X1_RVT
        port map (
            A1   => lr,
            A2   => s,
            Y    => cl_and_in);
    cl_and_A: AND2X1_RVT
        port map (
            A1   => la_tmp,
            A2   => cl_or_in,
            Y    => cl_anda);
    cl_and_C: AND2X1_RVT
        port map (
            A1   => cl_or_in,
            A2   => cl_and_in,
            Y    => cl_andc);
    cl_and_B: AND2X1_RVT
        port map (
            A1   => cl_and_in,
            A2   => la_tmp,
            Y    => cl_andb);
    cl_or_Z: OR3X1_RVT
        port map (
            A1   => cl_anda,
            A2   => cl_andc,
            A3   => cl_andb,
            Y    => cl_or_out);
    cl_gate_R: AND2X1_RVT
        port map (
            A1   => cl_or_out,
            A2   => reset,
            Y    => la_tmp);


    or_laps: OR2X1_RVT
        port map (
            A1   => s,
            A2   => la_tmp,
            Y    => laps);

-- Cr
    cr_and_I: AND2X1_RVT
        port map (
            A1   => ra,
            A2   => laps,
            Y    => cr_and_in);
    cr_and_A: AND2X1_RVT
        port map (
            A1   => rri,
            A2   => laps,
            Y    => cr_anda);
    cr_and_C: AND2X1_RVT
        port map (
            A1   => laps,
            A2   => cr_and_in,
            Y    => cr_andc);
    cr_and_B: AND2X1_RVT
        port map (
            A1   => cr_and_in,
            A2   => rri,
            Y    => cr_andb);
    cr_or_Z: OR3X1_RVT
        port map (
            A1   => cr_anda,
            A2   => cr_andc,
            A3   => cr_andb,
            Y    => cr_or_out);

-- Cs
    cs_or_I: OR2X1_RVT
        port map (
            A1   => lai,
            A2   => rr_tmp,
            Y    => cs_or_in);
    cs_and_A: AND2X1_RVT
        port map (
            A1   => s,
            A2   => cs_or_in,
            Y    => cs_anda);
    cs_and_C: AND2X1_RVT
        port map (
            A1   => cs_or_in,
            A2   => rr_tmp,
            Y    => cs_andc);
    cs_and_B: AND2X1_RVT
        port map (
            A1   => rr_tmp,
            A2   => s,
            Y    => cs_andb);
    cs_or_Z: OR3X1_RVT
        port map (
            A1   => cs_anda,
            A2   => cs_andc,
            A3   => cs_andb,
            Y    => cs_or_out);
    cs_gate_R: OR2X1_RVT
        port map (
            A1   => cs_or_out,
            A2   => reseti,
            Y    => s);

    rr <= rr_tmp;
    la <= la_tmp;
    clk <= lai;

    WITH_RST: if init_full generate
        cr_gate_R: AND2X1_RVT
            port map (
                A1   => cr_or_out,
                A2   => reset,
                Y    => rri);
    end generate WITH_RST;

    WOUT_RST: if not init_full generate
        cr_gate_R: OR2X1_RVT
            port map (
                A1   => cr_or_out,
                A2   => reseti,
                Y    => rri);
    end generate WOUT_RST;

end architecture maximus;
