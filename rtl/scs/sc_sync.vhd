library ieee;
use ieee.std_logic_1164.all;

library work;

entity sc is
    generic (
        init_full : boolean := false
        );
    port (
        lr : in std_logic;
        la : out std_logic;
        rr : out std_logic;
        ra : in std_logic;
        clk : out std_logic;
        reset : in std_logic
        );
end entity sc;

architecture sync of sc is

begin  -- sync

  clk <= lr;
  rr <= lr;
  la <= lr;

end architecture sync;
