library ieee;
use ieee.std_logic_1164.all;

library work;

entity sc is
    generic (
        init_full : boolean := false
        );
    port (
        lr : in std_logic;
        la : out std_logic;
        rr : out std_logic;
        ra : in std_logic;
        clk : out std_logic;
        reset : in std_logic
        );
end entity sc;

architecture burst_mode of sc is

    component OR3X1_RVT is
        port (
            A1 : in std_logic;
            A2 : in std_logic;
            A3 : in std_logic;
            Y  : out std_logic);
    end component;
    component NAND3X1_RVT is
        port (
            A1 : in std_logic;
            A2 : in std_logic;
            A3 : in std_logic;
            Y  : out std_logic);
    end component;
    component OR2X1_RVT is
        port (
            A1 : in std_logic;
            A2 : in std_logic;
            Y  : out std_logic);
    end component;
    component AND2X1_RVT is
        port (
            A1 : in std_logic;
            A2 : in std_logic;
            Y  : out std_logic);
    end component;
    component INVX1_RVT is
        port (
            A : in std_logic;
            Y : out std_logic);
    end component;
    component NBUFFX2_RVT is
        port (
            A : in std_logic;
            Y : out std_logic);
    end component;

    signal rr_tmp, rr_tmp2, rai, resetb, lai, rri, lr_tmp: std_logic;
    signal anda, andb, andc, or_out, or_rst : std_logic;        
    signal and_in, or_in : std_logic;        

begin  -- burst_mode

    inv_rst : INVX1_RVT
        port map (
            A => reset,
            Y => resetb);
    buff_lr : NBUFFX2_RVT
        port map (
            A => lr,
            Y => lr_tmp);
    inv_ra : INVX1_RVT
        port map (
            A => ra,
            Y => rai);
    inv_la : INVX1_RVT
        port map (
            A => or_rst,
            Y => lai);
    inv_rr : INVX1_RVT
        port map (
            A => rr_tmp2,
            Y => rri);
    and_rr: NAND3X1_RVT
        port map (
            A1   => lai,
            A2   => lr_tmp,
            A3   => ra,
            Y    => rr_tmp);
    rr <= rr_tmp2;
    rr_tmp2 <= rr_tmp after 2 ps;
    --rr_tmp2 <= rr_tmp;
    clk <= lr_tmp;
    la <= or_rst;

    WITH_RST: if init_full generate
        or_I: OR2X1_RVT
            port map (
                A1   => lr_tmp,
                A2   => rri,
                Y    => or_in);
        and_I: AND2X1_RVT
            port map (
                A1   => rri,
                A2   => rai,
                Y    => and_in);
        and_A: AND2X1_RVT
            port map (
                A1   => or_rst,
                A2   => or_in,
                Y    => anda);
        and_C: AND2X1_RVT
            port map (
                A1   => or_in,
                A2   => and_in,
                Y    => andc);
        and_B: AND2X1_RVT
            port map (
                A1   => and_in,
                A2   => or_rst,
                Y    => andb);
        and_R: AND2X1_RVT
            port map (
                A1   => or_out,
                A2   => reset,
                Y    => or_rst);
        or_Z: OR3X1_RVT
            port map (
                A1   => anda,
                A2   => andc,
                A3   => andb,
                Y    => or_out);
    end generate WITH_RST;

    WOUT_RST: if not init_full generate
        or_I: OR2X1_RVT
            port map (
                A1   => lr_tmp,
                A2   => rri,
                Y    => or_in);
        and_I: AND2X1_RVT
            port map (
                A1   => rri,
                A2   => rai,
                Y    => and_in);
        and_A: AND2X1_RVT
            port map (
                A1   => or_rst,
                A2   => or_in,
                Y    => anda);
        and_C: AND2X1_RVT
            port map (
                A1   => or_in,
                A2   => and_in,
                Y    => andc);
        and_B: AND2X1_RVT
            port map (
                A1   => and_in,
                A2   => or_rst,
                Y    => andb);
        and_R: OR2X1_RVT
            port map (
                A1   => or_out,
                A2   => resetb,
                Y    => or_rst);
        or_Z: OR3X1_RVT
            port map (
                A1   => anda,
                A2   => andc,
                A3   => andb,
                Y    => or_out);
    end generate WOUT_RST;

end architecture burst_mode;
