library ieee;
use ieee.std_logic_1164.all;

library work;

entity sc is
    generic (
        init_full : boolean := false
        );
    port (
        lr : in std_logic;
        la : out std_logic;
        rr : out std_logic;
        ra : in std_logic;
        clk : out std_logic;
        reset : in std_logic
        );
end entity sc;

architecture mousetrap of sc is


    component MUX21X1_RVT is
        port (
            A1 : in std_logic;
            A2 : in std_logic;
            S0 : in std_logic;
            Y  : out std_logic);
    end component;
    component AND2X1_RVT is
        port (
            A1 : in std_logic;
            A2 : in std_logic;
            Y  : out std_logic);
    end component;
    component XNOR2X1_RVT is
        port (
            A1 : in std_logic;
            A2 : in std_logic;
            Y  : out std_logic);
    end component;

    signal gate, trap, trap_rst : std_logic;

begin  -- mousetrap

    clk <= gate;
    rr <= trap_rst;
    la <= trap_rst;

    WITH_RST: if init_full generate
        ldc : MUX21X1_RVT 
            port map (
                A2 => lr,
                A1 => trap_rst,
                Y  => trap,
                S0 => gate);      
        andrst : AND2X1_RVT 
            port map (
                A1 => trap,
                A2 => reset,
                Y  => trap_rst);
        x_nor : XNOR2X1_RVT 
            port map (
                Y  => gate,
                A1 => trap_rst,
                A2 => ra);
    end generate WITH_RST;

    WOUT_RST: if not init_full generate
        ldc : MUX21X1_RVT 
            port map (
                A2 => lr,
                A1 => trap_rst,
                Y  => trap,
                S0 => gate);      
        andrst : AND2X1_RVT 
            port map (
                A1 => trap,
                A2 => reset,
                Y  => trap_rst);
        x_nor : XNOR2X1_RVT 
            port map (
                Y  => gate,
                A1 => trap_rst,
                A2 => ra);
    end generate WOUT_RST;

end architecture mousetrap;
