library ieee;
use ieee.std_logic_1164.all;

library work;

entity sc is
    generic (
        init_full : boolean := false
        );
    port (
        lr : in std_logic;
        la : out std_logic;
        rr : out std_logic;
        ra : in std_logic;
        clk : out std_logic;
        reset : in std_logic
        );
end entity sc;

architecture click of sc is

    component INVX1_RVT is
        port (
            A : in std_logic;
            Y : out std_logic);
    end component;
    component DFFARX1_RVT is
        port (
            D    : in std_logic; 
            CLK  : in std_logic;
            RSTB : in std_logic;
            Q    : out std_logic);
    end component;
    component AND3X1_RVT is
        port (
            A1 : in std_logic;
            A2 : in std_logic;
            A3 : in std_logic;
            Y  : out std_logic);
    end component;
    component OR2X1_RVT is
            port (
                A1 : in std_logic;
                A2 : in std_logic;
                Y  : out std_logic);
    end component;

    signal gate, lri, rai, click, clicki, n_or1, n_or2 : std_logic;

begin  -- click

    clk <= gate;
    rr <= click;
    la <= click;

    WITH_RST: if init_full generate
        ffc : DFFARX1_RVT
            port map (
                D => clicki, 
                CLK => gate,
                RSTB => reset,
                Q => click);
        ctl_and1 : AND3X1_RVT
            port map (
                A1 => lri,
                A2 => click,
                A3 => ra,
                Y => n_or1);
        ctl_and2 : AND3X1_RVT
            port map (
                A1 => lr,
                A2 => clicki,
                A3 => rai,
                Y => n_or2);
        ctl_or   : OR2X1_RVT
            port map (
                A1 => n_or1,
                A2 => n_or2,
                Y => gate);
        ctl_inv_r :  INVX1_RVT
            port map (
                A => lr,
                Y => lri);
        ctl_inv_a :  INVX1_RVT
            port map (
                A => ra,
                Y => rai);
        ctl_inv_c :  INVX1_RVT
            port map (
                A => click,
                Y => clicki); 
    end generate WITH_RST;

    WOUT_RST: if not init_full generate
        ffc : DFFARX1_RVT
            port map (
                D => clicki, 
                CLK => gate,
                RSTB => reset,
                Q => click);
        ctl_and1 : AND3X1_RVT
            port map (
                A1 => lri,
                A2 => click,
                A3 => ra,
                Y => n_or1);
        ctl_and2 : AND3X1_RVT
            port map (
                A1 => lr,
                A2 => clicki,
                A3 => rai,
                Y => n_or2);
        ctl_or   : OR2X1_RVT
            port map (
                A1 => n_or1,
                A2 => n_or2,
                Y => gate);
        ctl_inv_r :  INVX1_RVT
            port map (
                A => lr,
                Y => lri);
        ctl_inv_a :  INVX1_RVT
            port map (
                A => ra,
                Y => rai);
        ctl_inv_c :  INVX1_RVT
            port map (
                A => click,
                Y => clicki); 
    end generate WOUT_RST;

end architecture click;
