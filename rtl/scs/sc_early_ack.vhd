library ieee;
use ieee.std_logic_1164.all;

library work;

entity sc is
    generic (
        init_full : boolean := false
        );
    port (
        lr : in std_logic;
        la : out std_logic;
        rr : out std_logic;
        ra : in std_logic;
        clk : out std_logic;
        reset : in std_logic
        );
end entity sc;

architecture early_ack of sc is

    component OR2X1_RVT is
        port (
            A1 : in std_logic;
            A2 : in std_logic;
            Y  : out std_logic);
    end component;
    component AND2X1_RVT is
        port (
            A1 : in std_logic;
            A2 : in std_logic;
            Y  : out std_logic);
    end component;
    component INVX1_RVT is
        port (
            A : in std_logic;
            Y : out std_logic);
    end component;
    component OR3X1_RVT is
        port (
            A1 : in std_logic;
            A2 : in std_logic;
            A3 : in std_logic;
            Y  : out std_logic);
    end component;
    component NBUFFX2_RVT is
        port (
            A : in std_logic;
            Y : out std_logic);
    end component;


    signal reseti, complete, rst, lr_tmp, la_tmp, la_tmp_trd1, la_tmp_trd2, la_tmp_trd3, rab : std_logic;
    signal anda, andb, andc, or_out, or_rst, rai, lri, rri : std_logic;        

begin  -- early_ack

    la <= la_tmp;
    rr <= or_rst;

    buff_lr : NBUFFX2_RVT
        port map (
            A => lr,
            Y => lr_tmp);
    buff_TRD1 : NBUFFX2_RVT
        port map (
            A => la_tmp,
            Y => la_tmp_trd1);
    buff_TRD2 : NBUFFX2_RVT
        port map (
            A => la_tmp_trd1,
            Y => la_tmp_trd2);
    buff_TRD3 : NBUFFX2_RVT
        port map (
            A => la_tmp_trd2,
            Y => la_tmp_trd3);
    or_TRD: OR2X1_RVT
        port map (
            A1   => la_tmp,
            A2   => la_tmp_trd3,
            Y    => rst);
    inv_lr: INVX1_RVT
        port map (
            A    => lr_tmp,
            Y    => lri);
    inv_rr: INVX1_RVT
        port map (
            A    => or_rst,
            Y    => rri);
    inv_la: INVX1_RVT
        port map (
            A    => la_tmp,
            Y    => clk);
    and_A1: AND2X1_RVT
        port map (
            A1   => rri,
            A2   => lr_tmp,
            Y    => la_tmp);
    and_A2: AND2X1_RVT
        port map (
            A1   => rst,
            A2   => lri,
            Y    => complete);
    or_Z: OR3X1_RVT
        port map (
            A1   => anda,
            A2   => andc,
            A3   => andb,
            Y    => or_out);
    and_A: AND2X1_RVT
        port map (
            A1   => or_rst,
            A2   => complete,
            Y    => anda);
    and_C: AND2X1_RVT
        port map (
            A1   => complete,
            A2   => rai,
            Y    => andc);
    and_B: AND2X1_RVT
        port map (
            A1   => rai,
            A2   => or_rst,
            Y    => andb);
    inv_ra: INVX1_RVT
        port map (
            A    => ra,
            Y    => rai);

    WITH_RST: if init_full generate
        gate_R: OR2X1_RVT
            port map (
                A1   => or_out,
                A2   => reseti,
                Y    => or_rst);
        inv_rst: INVX1_RVT
            port map (
                A    => reset,
                Y    => reseti);
    end generate WITH_RST;
    
    WOUT_RST: if not init_full generate
        gate_R: AND2X1_RVT
            port map (
                A1   => or_out,
                A2   => reset,
                Y    => or_rst);
    end generate WOUT_RST;    
    
end architecture early_ack;
