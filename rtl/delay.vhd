library ieee;
use ieee.std_logic_1164.all;

entity delay is

  port (
    D : in  std_logic;
    Q : out std_logic);

end delay;

architecture saed of delay is

    component INVX1_RVT is
        port (
            A : in std_logic;
            Y : out std_logic
            );
    end component;
 
begin  -- saed

    inv : INVX1_RVT
        port map (
            A => D,
            Y => Q
            );

--    Q <= not D after 10 ps;
  
end architecture saed;
