# Local Clock Set Benchmark

*A &nbsp;<a href="http://tima.univ-grenoble-alpes.fr"><img src="http://tima.univ-grenoble-alpes.fr/tima/ui/images/communs/logoTIMA.png" alt="drawing" height="50"/></a>&nbsp; and 
<a href="http://www.icalps.com"><img src="https://www.icalps.com/wp-content/uploads/2020/02/logo.png" alt="drawing" height="50"/></a> project.*


## Static Timing Analysis of Bundled-data circuits

The Local Clock Set methodology provides the asynchronous community with an efficient and robust method to perform the Static Timing Analysis of bundled-data circuits using standard clocked EDA tools.

For more information on the LCS methodology, please refer to :  
G. Gimenez, A. Cherkaoui, G. Cogniard, and L. Fesquet, "Static timing analysis of asynchronous bundled-data circuits," in *24th IEEE International Symposium on Asynchronous Circuits and Systems*, May 2018. DOI:10.1109/ASYNC.2018.00036

## A benchmark

This LCS Benchmark repository gives an implementation example of several bundled-data templates along with the associated LCS rules to generate their timing constraints files (SDC format).

Nine circuit schemes are considered in this benchmark:
- synchronous
- reactive clock
- WCHB
- Burst mode
- Early acknowledgment
- Maximus
- Micropipeline
- Mousetrap
- Click

At the moment this project only deal with the *Bundling* and *Data-overrun* timing constraints.

The benchmark results are presented in :  
G. Gimenez, J. Simatic, and L. Fesquet, "From Signal Transition Graphs to Timing Closure: Application to Bundled-Data Circuits," in *25th IEEE International Symposium on Asynchronous Circuits and Systems*, May 2019. to be published

## Instructions

### Repository organisation

```
 LocalClockSetBenchmark
  ├── backbone		# backbone scripts used in the benchmark (for synthesis and timing constraints generation)  
  ├── lcs  
  │   ├── pipe.tcl	# LCS description of the benchmark circuit (list of the register controller)  
  │   └── rules		# LCS rules file of each template  
  ├── libraries		# technology libraries (in our case saed32nm rvt)  
  ├── rtl		# rtl source of the benchmark circuit		  
  │   └── scs		# rtl source of the different templates (mapped to the saed32nm library)  
  ├── LICENSE		# License file (MIT)  
  ├── README.md		# Readme file  
  └── run		# script to launch the benchmark  
```

### Tools

The Benchmark uses Synopsys DesignCompiler and Synopsys PrimeTime tools. You must have appropriate license to run these tools.

### Library

The benchmark targets Synopsys saed32nm (RVT) libraries available [on solvnet](https://solvnet.synopsys.com/retrieve/customer/application_notes/attached_files/2630223/bitcoin_v1.1_lib.tgz).
If you don't have access to these libraries, you may update the controllers implementation and LCS rules to match another library of your choice.

## Contributing

This project is an open source project under MIT license. Feel free to add your contribution to this project, to add new templates or to implement others types of timing constraints.

## Acknowledgments

This work has been supported by [IC'Alps](http://www.icalps.com) and the [TIMA Laboratory](http://tima.univ-grenoble-alpes.fr).
