###############
# LCS CONTROL STRUCTURE LIST  
###############
# Design : Pipe
# Author : Grégoire Gimenez
# Date   : 2019/02/28
###############

if { ! [info exist env(pipe_size)] } {
  puts "ERROR: Could not find pipe_size env variable"
  exit 0
}

## Register controller list and name of the associated root clock to be created
# format : <root clock name> {<controller hier path> <budgeted clock period>}
array set LCS_reg_ctrl_list ""
for {set i 0} {$i < $env(pipe_size)} {incr i} {
  for {set j 0} {$j < 5} {incr j} {
    set LCS_reg_ctrl_list(RC$i$j) "p5_i_$i/sc_i_$j 0.0001"
  }
}

