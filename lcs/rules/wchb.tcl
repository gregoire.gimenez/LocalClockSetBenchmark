###############
# LCS RULES - WCHB Scheme 
###############
# Author : Grégoire Gimenez
# Date   : 2019/02/28
###############
# Rev 0.1
#   - initial release of the LCS rules
###############
#
## Register controller schematic:
######
#
#             ,-----------------------.
#             |   _         rstb      |
#             `--| \   ____   |       |
#                |&A)--\   \  |   _   |
#    lr ------,--|_/    |   \ `--| \  |
#             `--| \    |    \   |&R)-+------ (la,rr,clk)
#                |&C)---|OR_Z )--|_/  |
#             ,--|_/    |    /        |
#    ra --|>o-'--| \    |   /         |
#                |&B)--/___/          |
#             ,--|_/                  |
#             |                       |
#             `-----------------------'
# 
## LCS tree:
######
#              
#    Stage i-1      :      Stage i      :      Stage i+1
#                   :                   :
#                   :        RCi -------:---> RCi_capture
#                   :    (root clock)   :
#                   :         |         :
#   RCi_phase2 <----:---------'         :
#      (inv)        :                   :
#        |          :                   :
#        `----------:--> RCi_phase3     :
#                   :         |         :
#   RCi_launch <----:---------'         :
#      (inv)        :                   :
#                   :                   :
#
## Valid paths:
######
# * setup: RCi -> RCi_capture
# * hold:  RCi_launch -> RCi
#
## Clock interactions table:
######
#    (X->false path, H->active hold, S-> active setup)
# vfrom/to>           1   2   3   4
#  RCi          - 1   X   S   X   X
#  RCi_capture  - 2   X   X   X   X
#  RCi_launch   - 3   H   X   X   X
#  RCi_phase*   - 4   X   X   X   X 
#

## Check that LCS register controller list has been previously defined
if { ! [info exist LCS_reg_ctrl_list] } {
  puts "ERROR: Could not find any register controller list"
  exit 0
}

source [file join [file dirname [info script]] functions.tcl]


#####
## First step : create event on each reg controller
#####

# Register controllers
foreach {clk ctrl_period} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl_period 0]
  set period [lindex $ctrl_period 1]
  ## Create root clock at the output of the muller cell (= output pin Y of the OR_Z gate)
  create_clock -name ${clk} -period ${period} [get_pin ${ctrl}/or_Z/Y]
}


#####
## Second.0 step: create generated clock for phase 2 (hold)
#####

# update_timing required to propagate actual root clocks
update_timing

# Initialize table
array set genclock_table_hold ""

# get clock relations for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  # Get root clock propagating to right acknowledge pin of each register controller.
  set genclock_table_hold($clk) [get_attribute [get_attribute -quiet [get_pin ${ctrl}/inv_I/A] clocks] full_name] 
}

# create generated clock for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  foreach genclk $genclock_table_hold($clk) {
    ## Create event propagation clock (EPC) from the right acknowledge port (= input pin A of the INV_I gate) to the output of the muller cell (= output pin Y of the OR_Z gate)
    #   (when creating clock it is always a good idea to point to std cell pin instead of hierarchical pin to avoid any optimization and port renaming issue...)
    create_generated_clock -name ${genclk}_phase2 -add -combinational -master [get_clocks ${genclk}] -divide_by 1 -invert -source [get_pin ${ctrl}/inv_I/A] [get_pin ${ctrl}/or_Z/Y]
  }
}


#####
## Second.1 step : create generated clock for phase 3 (hold)
#####

# update_timing required to propagate actual phase 2 clocks
update_timing

# Initialize table
array set genclock_table_hold ""

# get clock relations for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  # Get phase2 clock propagating to left request input of each register controller.
  set genclock_table_hold($clk) [get_attribute [filter_collection [get_attribute -quiet [get_pin ${ctrl}/and_A/A2] clocks] "full_name=~*phase2*"] full_name] 
}

# create generated clock for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  foreach genclk $genclock_table_hold($clk) {
    ## Create event propagation clock (EPC) from the left request port (= input pin A2 of the AND_A gate) to the output of the muller cell (= output pin Y of the OR_Z gate)
    #   (when creating clock it is always a good idea to point to std cell pin instead of hierarchical pin to avoid any optimization and port renaming issue...)
    create_generated_clock -name [regsub "phase2" ${genclk} "phase3"] -add -combinational -master [get_clocks ${genclk}] -divide_by 1 -source [get_pin ${ctrl}/and_A/A2] [get_pin ${ctrl}/or_Z/Y]
  }
}


#####
## Second.2 step: create generated clock for launch (hold) and capture (setup)
#####

# update_timing required to propagate actual phase3 clocks
update_timing

# Initialize tables
array set genclock_table_hold ""
array set genclock_table_setup ""

# get clock relations for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  # Get phase3 clock propagating to right acknowledge pin of each register controller.
  set genclock_table_hold($clk) [get_attribute [filter_collection [get_attribute -quiet [get_pin ${ctrl}/inv_I/A] clocks] "full_name=~*phase3*"] full_name] 
  # Get root clock propagating to left request pin of each register controller.
  set genclock_table_setup($clk) [get_attribute [filter_collection [get_attribute -quiet [get_pin ${ctrl}/and_A/A2] clocks] "full_name!~*phase*"] full_name] 
}

# create generated clock for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  foreach genclk $genclock_table_hold($clk) {
    ## Create launch clock from the right acknowledge port (= input pin A of the INV_I gate) to the output of the muller cell (= output pin Y of the OR_Z igate)
    #   (when creating clock it is always a good idea to point to std cell pin insntead of hierarchical pin to avoid any optimization and port renaming issue...)
    create_generated_clock -name [regsub "phase3" ${genclk} "launch"] -add -combinational -master [get_clocks ${genclk}] -divide_by 1 -invert -source [get_pin ${ctrl}/inv_I/A] [get_pin ${ctrl}/or_Z/Y]
  }
  foreach genclk $genclock_table_setup($clk) {
    ## Create capture clock from the left request port (= input pin A2 of the AND_A gate) to the output of the muller cell (= output pin Y of the OR_Z gate)
    #   (when creating clock it is always a good idea to point to std cell pin instead of hierarchical pin to avoid any optimization and port renaming issue...)
    create_generated_clock -name ${genclk}_capture -add -combinational -master [get_clocks ${genclk}] -divide_by 1 -source [get_pin ${ctrl}/and_A/A2] [get_pin ${ctrl}/or_Z/Y]
  }
}


#####
## Third step: define exceptions
#####

## Fully asynchronous timing
set_multicycle_path -1 -hold  -from [get_clocks *] -to [get_clocks *]
set_multicycle_path 0 -setup -from [get_clocks *] -to [get_clocks *]


## Disable inter LCS paths
set cmd "set_clock_group -asynchronous "
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  lappend cmd "-group \[trace_generated_clock -flat ${clk}\]"
}
echo [join $cmd]
eval [join $cmd]


## Disable intra LCS paths (=> see clock interactions table)
# 1/ Disable paths from/to the same root clock (a root clock can not be both launch and capture clock of the same LCS) => cell 1-1 
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set_false_path -from [get_clocks $clk] -to [get_clocks $clk]
}
# 2/ Disable timing to generated clocks that do not capture any path => columns 4 and 3
set_false_path -to [get_clocks "*_launch *phase*"]
# 3/ Disable timing from generated clocks that do not launch any path => rows 2 and 4
set_false_path -from [get_clocks "*phase* *_capture"]
# 4/ Disable setup check for clock dedicated to hold check => cells 3-1 and 3-2
set_false_path -setup -from [get_clocks -quiet "*_launch"]
# 5/ Disable hold check for clock dedicated to setup check => cells 1-2 and 3-2
set_false_path -hold -to [get_clocks -quiet "*_capture"]


#####
## Others...
#####
#
# propage clock to get effective timings
set_propagated_clock [all_clocks]

# Do not infer clock gating check for now...
set timing_disable_clock_gating_checks true

