###############
# LCS Useful Functions 
###############
# Author : Grégoire Gimenez
# Date   : 2019/02/28
###############
# Rev 0.1
#   - initial release of the LCS rules
###############

## proc to get master clocks of a clock
###############
proc get_master_clocks { args } {

  set results(-level) 0
  set results(-flat) 0
  set results(-all) 0
  parse_proc_arguments -arg $args results

  proc get_mclocks { clock level flat cur_level } { 
    ## master_clock attribute is not defined in DesignCompiler (L-2016.03-SP5-4)
    if { $::synopsys_program_name == "dc_shell" } {
      set test [get_clock [get_attribute $clock master_clock_name -quiet]]
    } else {
      set test [get_attribute $clock master_clock -quiet]
    }
    if { ($level > 0 && $level == $cur_level) || ![sizeof_collection $test] } {
      return $clock 
    } else {
      if { $flat && $cur_level } {
        return [add_to_collection $clock [get_mclocks $test $level $flat [expr $cur_level + 1]]]
      } else {
        return [get_mclocks $test $level $flat [expr $cur_level + 1]]
      }
    }
  }

  set res ""
  foreach_in_collection clk [get_clock $results(clock)] {
    set res [add_to_collection $res [get_mclocks [get_clock $clk] $results(-level) $results(-flat) 0]]
  }
  if { !$results(-all) } {
    return [get_clocks [lsort -uniq [get_attribute $res full_name]]]
  } else {
    return [get_clocks $res]
  }
}

define_proc_attributes get_master_clocks \
  -info "Get the master clock of a specific clock. When '-flat' option is specified, \r
                        all master clocks are recursively traced. '-level' option allows to specify \r
                        the master clock level to be reported. When both options are specified all \r
                        master clocks are reported recursively up to the specified level.\r
			" \
  -define_args {
    {clock "Clock for which master clock must be retrieved" clock string required}
    {-flat "Trace all master clocks recursively" "" boolean optional}
    {-level "Number of level to trace" level int optional}
    {-all "Don't uniquify reported clocks" "" boolean optional}
  }

## proc to get generated clocks from a clock
###############
proc trace_generated_clocks { args } {

  set results(-level) 0
  set results(-flat) 0
  parse_proc_arguments -arg $args results

  proc get_gclocks { clock level flat cur_level } { 
    set gclk [get_attribute [get_clock -quiet $clock] generated_clocks -quiet]
    if { ($level == 0 && !$flat) } {
    ## level = 0 and flat = 0 is the default behavior and correspond to level = 1 flat = 0
      return $gclk
    } elseif { ![sizeof_collection $gclk] } {
    ## if no generated clock exist for the current clocks
      if { !$flat } {
      # return the current clock when not flat mode
        return $clock
      } else {
      # when flat mode no return required (avoid double entry of the last generated clocks)
        return ""
      }
    } elseif { $cur_level == $level } {
    ## we reach the requested level number, return the generated clocks
      return $gclk
    } elseif { $flat } { 
    ## when flat mode, concatenate the generated clock with the next generated clock level
      return [add_to_collection $gclk [get_gclocks $gclk $level $flat [expr $cur_level+1]]]
    } else {
    ## else return the next generated clock level
      return [get_gclocks $gclk $level $flat [expr $cur_level+1]]
    }
  }
  return [add_to_collection [get_clocks $results(clock)] [get_gclocks [get_clocks $results(clock)] $results(-level) $results(-flat) 1]]
}


define_proc_attributes trace_generated_clocks \
  -info "Get the generated clocks of a specific clock. When '-flat' option is specified, \r
                        all generated clocks are recursively traced. '-level' option allows to specify \r
                        the generated clock level to be reported. When both options are specified all \r
                        generated clocks are reported recursively downto the specified level.\r
			" \
  -define_args {
    {clock "Name of the clock for which generated clock must be retrieved" clock string required}
    {-flat "Trace all generated clocks recursively" "" boolean optional}
    {-level "Number of levels to be crossed" level int optional}
  }

