###############
# LCS RULES - Simple (Micropipeline) Scheme 
###############
# Author : Grégoire Gimenez
# Date   : 2019/02/28
###############
# Rev 0.1
#   - initial release of the LCS rules
###############
#
## Register controller schematic:
######
#
#             ,-----------------------.
#             |   _         rstb      |
#             `--| \   ____   |       |
#                |&A)--\   \  |   _   |
#    lr ------,--|_/    |   \ `--| \  |
#             `--| \    |    \   |&R)-+------ (la,rr,clk)
#                |&C)---|OR_Z )--|_/  |
#             ,--|_/    |    /        |
#    ra --|>o-'--| \    |   /         |
#                |&B)--/___/          |
#             ,--|_/                  |
#             |                       |
#             `-----------------------'
# 
## LCS tree:
######
#
#    Stage i-1      :      Stage i      :      Stage i+1
#                   :                   :
#                   :        RCi -------:---> RCi_capture
#                   :    (root clock)   :
#                   :         |         :
#   RCi_launch <----:---------'         :
#      (inv)        :                   :
#                   :                   :
#
## Valid paths:
######
# * setup: RCi (rise) -> RCi_capture (rise)
#          RCi (fall) -> RCi_capture (fall)
# * hold:  RCi_launch (fall) -> RCi (rise)
#          RCi_launch (rise) -> RCi (fall)
#
## Clock interactions table:
######
#    (X->false path, H->active hold, S-> active setup)
# vfrom/to>                 1   2   3   4   5   6
#  RCi (rise)         - 1   X   X   S   X   X   X
#  RCi (fall)         - 2   X   X   X   S   X   X
#  RCi_capture (rise) - 3   X   X   X   X   X   X
#  RCi_capture (fall) - 4   X   X   X   X   X   X
#  RCi_launch (rise)  - 5   X   H   X   X   X   X
#  RCi_launch (fall)  - 6   H   X   X   X   X   X
#  


## Check that LCS register controller list has been previously defined
if { ! [info exist LCS_reg_ctrl_list] } {
  puts "ERROR: Could not find any register controller list"
  exit 0
}

source [file join [file dirname [info script]] functions.tcl]


#####
## First step : create event on each reg controller
#####

# Register controllers
foreach {clk ctrl_period} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl_period 0]
  set period [lindex $ctrl_period 1]
  ## Create root clock at the output of the muller cell (= output pin Y of the OR_Z gate)
  create_clock -name ${clk} -period ${period} [get_pin ${ctrl}/or_Z/Y]
}


#####
## Second step: create generated clocks for launch (hold) and capture (setup)
#####

# update_timing required to propagate actual root clocks
update_timing

# Initialize tables
array set genclock_table_hold ""
array set genclock_table_setup ""

# get clock relations for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  # Get root clock propagating to right acknowledge pin of each register controller.
  set genclock_table_hold($clk) [get_attribute [get_attribute -quiet [get_pin ${ctrl}/inv_I/A] clocks] full_name] 
  # Get root clock propagating to left request pin of each register controller.
  set genclock_table_setup($clk) [get_attribute [get_attribute -quiet [get_pin ${ctrl}/and_A/A2] clocks] full_name] 
}

# create generated clock for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  foreach genclk $genclock_table_hold($clk) {
    ## Create launch clock from the right acknowledge port (= input pin A of the INV_I gate) to the output of the muller cell (= output pin Y of the OR_Z igate)
    #   (when creating clock it is always a good idea to point to std cell pin insntead of hierarchical pin to avoid any optimization and port renaming issue...)
    create_generated_clock -name ${genclk}_launch -add -combinational -master [get_clocks ${genclk}] -divide_by 1 -invert -source [get_pin ${ctrl}/inv_I/A] [get_pin ${ctrl}/or_Z/Y]
  }
  foreach genclk $genclock_table_setup($clk) {
    ## Create capture clock from the left request port (= input pin A2 of the AND_A gate) to the output of the muller cell (= output pin Y of the OR_Z gate)
    #   (when creating clock it is always a good idea to point to std cell pin instead of hierarchical pin to avoid any optimization and port renaming issue...)
    create_generated_clock -name ${genclk}_capture -add -combinational -master [get_clocks ${genclk}] -divide_by 1 -source [get_pin ${ctrl}/and_A/A2] [get_pin ${ctrl}/or_Z/Y]
  }
}


#####
## Third step: define exceptions
#####

## Fully asynchronous timing
set_multicycle_path -1 -hold  -from [get_clocks *] -to [get_clocks *]
set_multicycle_path 0 -setup -from [get_clocks *] -to [get_clocks *]


## Disable inter LCS paths
set cmd "set_clock_group -asynchronous "
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  lappend cmd "-group \[trace_generated_clock -flat ${clk}\]"
}
echo [join $cmd]
eval [join $cmd]


## Disable intra LCS paths (see clock interactions table)
# 1/ Disable paths from/to the same root clock (a root clock can not be both launch and capture clock of the same LCS) => box 1-1 to 2-2
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set_false_path -from [get_clocks $clk] -to [get_clocks $clk]
}
# 2/ Disable paths between opposite edge in a same setup LCS (XOR phase conversion block allows both edge conversion) => cells 1-4 and 2-3
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  if { [sizeof_collection [get_clocks -quiet ${clk}_capture]] } {
    set_false_path -rise_from [get_clocks $clk] -fall_to [get_clocks ${clk}_capture]
    set_false_path -fall_from [get_clocks $clk] -rise_to [get_clocks ${clk}_capture]
  }
}
# 3/ Disable paths between same edge in a same hold LCS (XOR phase conversion block allows both edge conversion) => cells 5-1 and 6-2
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  if { [sizeof_collection [get_clocks -quiet ${clk}_launch]] } {
    set_false_path -rise_from [get_clocks ${clk}_launch] -rise_to [get_clocks ${clk}]
    set_false_path -fall_from [get_clocks ${clk}_launch] -fall_to [get_clocks ${clk}]
  }
}
# 4/ Disable path to each launch clock that do not capture => column 5 and 6
set_false_path -to [get_clocks "*_launch"]
# 5/ Disable timing from each capture clock that do not launch => rows 3 and 4
set_false_path -from [get_clocks "*_capture"]
# 6/ Disable setup check for clock dedicated to hold check => cells 6-1 and 5-2
set_false_path -setup -from [get_clocks "*_launch"]
# 7/ Disable hold check for clock dedicated to setup check => cells 1-3 and 2-4
set_false_path -hold -to [get_clocks -quiet "*_capture"]


#####
## Others...
#####

# propage clock to get effective timings
set_propagated_clock [all_clocks]

# Do not infer clock gating check for now...
set timing_disable_clock_gating_checks true

