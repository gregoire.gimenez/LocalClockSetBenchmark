###############
# LCS RULES - Mousetrap Scheme 
###############
# Author : Grégoire Gimenez
# Date   : 2019/02/28
###############
# Rev 0.1
#   - initial release of the LCS rules
###############
#
## Register controller schematic:
######
#                clk    _
#                 |    / //----.
#                 |--o(XNR|    |
#                 |    \_\\-.  |
#    la ----------(---------|  '--- ra
#                 | rstb_   |  
#               |'.  `-| \  |
#    lr --------|  |   |&R)-+------ rr
#               ldc|---|_/  |
#            ,--|  |        |
#            |  |.'         |
#            `--------------'
# 
## LCS tree:
######
#
#    Stage i-1       :           Stage i            :            Stage i+1             
#                    :                              :
#                    :     RCiS ---> RCi_capture ---:--> RCiS_phase2 --> RCiS_captureR
#                    : (root clk)                   :                `-> RCiS_captureF
#                    :                              :                
#                    :     RCiH ---> RCiH_captureR  :
#                    : (root clk)`-> RCiH_captureF  :
#                    :      |                       :
#   RCiH_launchR <---:------'                       :
#   RCiH_launchF <-' :                              :
#                    :                              :
#
## Valid paths:
######
# * setup: RCiS -> RCi_captureR (propagation of a rising edge on the request path)
#          RCiS -> RCi_captureF (propagation of a falling edge on the request path)
# * hold:  RCiH_launchR -> RCiH_captureR (propagation of a rising edge on the acknowledge path)
#          RCiH_launchRF-> RCiH_captureF (propagation of a falling edge on the acknowledge path)
#
## Clock interactions table:
######
#    (X->false path, H->active hold, S-> active setup)
# vfrom/to>             1   2   3   4   5   6   7   8   9
#  RCiS_phase*    - 1   X   X   X   X   X   X   X   X   X
#  RCiH           - 2   X   X   X   X   X   X   X   X   X
#  RCiS           - 3   X   X   X   S   S   X   X   X   X
#  RCiS_captureR  - 4   X   X   X   X   X   X   X   X   X
#  RCiS_captureF  - 5   X   X   X   X   X   X   X   X   X
#  RCiH_launchR   - 6   X   X   X   X   X   X   X   H   X
#  RCiH_launchF   - 7   X   X   X   X   X   X   X   X   H
#  RCiH_capture_R - 8   X   X   X   X   X   X   X   X   X
#  RCiH_capture_F - 9   X   X   X   X   X   X   X   X   X
#  

## Check that LCS register controller list has been previously defined
if { ! [info exist LCS_reg_ctrl_list] } {
  puts "ERROR: Could not find any register controller list"
  exit 0
}

source [file join [file dirname [info script]] functions.tcl]

#####
## First step : create event on each reg controller
#####
#
# Register controllers
foreach {clk ctrl_period} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl_period 0]
  set period [lindex $ctrl_period 1]
  ## Create root clock for setup at the output of the phase converter (= output pin Y of the XNOR gate)
  create_clock -name ${clk}S -period ${period} [get_pin ${ctrl}/x_nor/Y]
  ## Create root clock for hold at the output of the control latch (= output pin Y of the LDC gate)
  create_clock -name ${clk}H -period ${period} [get_pin ${ctrl}/ldc/Y]
  ## Propagate capture clock for setup (cross the hold root clock source)
  create_generated_clock -name ${clk}S_phase1  -edges {1 1 3} -source [get_pin ${ctrl}/ldc/S0]   [get_pin ${ctrl}/ldc/Y]    -master ${clk}S -add -combinational
  ## Propagate rising edge capture clock for hold (cross the setup root clock source)
  create_generated_clock -name ${clk}H_captureF -edges {1 2 3} -source [get_pin ${ctrl}/x_nor/A1] [get_pin ${ctrl}/x_nor/Y] -master ${clk}H -add -combinational
  ## Propagate falling edge capture clock for hold (cross the setup root clock source)
  create_generated_clock -name ${clk}H_captureR -edges {2 3 4} -source [get_pin ${ctrl}/x_nor/A1] [get_pin ${ctrl}/x_nor/Y] -master ${clk}H -add -combinational
  ## Disable internal loop of control latch (mux I0->Y). It confuses the crpr engine (RCHi directly loop to itself through this arc) but this arc is never used for timing analysis.
  set_disable_timing -from A1 -to Y [get_cell ${ctrl}/ldc]

}

#####
## Second step: create generated clocks for launch (hold) and capture (setup)
#####

# update_timing required to propagate actual clocks
update_timing

# Initialize tables
array set genclock_table_hold ""
array set genclock_table_setup ""

# get clock relations for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  # Get hold root clock propagating to right acknowledge pin of each register controller.
  set genclock_table_hold($clk) [get_attribute [get_clock -quiet [get_attribute -quiet [get_pin ${ctrl}/x_nor/A2] clocks] -filter "full_name=~*H"] full_name]
  # Get setup root clock propagating to left request pin of each register controller.
  set genclock_table_setup($clk) [get_attribute [get_clock -quiet [get_attribute -quiet [get_pin ${ctrl}/ldc/A2] clocks] -filter "full_name=~*S_phase1"] full_name]
}
  
# create generated clock for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  foreach genclk $genclock_table_hold($clk) {
    ## Create launch clocks from the right acknowledge port to the output of the phase conversion block - Propagate hold LCS through xnor $rc_pin/x_nor (A2->Y).
    #    Boths sense (rise and fall) must be propagated since xnor is an unate function."
    create_generated_clock -name ${genclk}_launchR -edges {1 2 3} -source [get_pin ${ctrl}/x_nor/A2] [get_pin ${ctrl}/x_nor/Y] -master ${genclk} -add
    create_generated_clock -name ${genclk}_launchF -edges {2 3 4} -source [get_pin ${ctrl}/x_nor/A2] [get_pin ${ctrl}/x_nor/Y] -master ${genclk} -add
  }
  foreach genclk $genclock_table_setup($clk) {
    ## Create phase 2 clock from the left request port to the output of the control latch - propagate setup LCS through latch $rc_pin/ldc (D->Q)
    create_generated_clock -name [regsub "phase1" ${genclk} "phase2"] -divide_by 1 -source [get_pin ${ctrl}/ldc/A2] [get_pin ${ctrl}/ldc/Y] -master ${genclk} -add -combinational
    # Propagate setup LCS through xnor ${ctrl}/x_nor (A1->Y). Both edges (rise and fall) must be propagated since xnor is an unate function.
    create_generated_clock -name [regsub "phase1" ${genclk} "captureF"] -edges {1 2 3} -source [get_pin ${ctrl}/x_nor/A1] [get_pin ${ctrl}/x_nor/Y] -master [regsub "phase1" ${genclk} "phase2"] -add -combinational
    create_generated_clock -name [regsub "phase1" ${genclk} "captureR"] -edges {2 3 4} -source [get_pin ${ctrl}/x_nor/A1] [get_pin ${ctrl}/x_nor/Y] -master [regsub "phase1" ${genclk} "phase2"] -add -combinational
  }
}

#####
## Third step: define exceptions
#####

# Fully asynchronous timing
set_multicycle_path -1 -hold  -from [get_clocks *] -to [get_clocks *]
set_multicycle_path 0 -setup -from [get_clocks *] -to [get_clocks *]


## Disable inter LCS paths
set group_cmd "set_clock_groups -asynchronous"
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  lappend group_cmd "-group \[get_clock -quiet \"${clk}S ${clk}S_capture*\"\] -group \[get_clock -quiet \"${clk}H ${clk}H_capture* ${clk}H_launch*\"\]"
}
echo [join $group_cmd]
eval [join $group_cmd]


## Disable intra LCS paths (=> see clock interactions table)
# 1/ Disable paths from/to event propagation clocks (phase*) => row 1 and column 1
set_false_path -from [get_clock *phase*]
set_false_path -to [get_clock *phase*]
# 2/ Disable paths from/to hold root clock (do not directly capture nor launch any timing path) => row 2 and column 2
set_false_path -from [get_clock *H]
set_false_path -to [get_clock *H]
# 3/ Disable path to setup root clock (do not directly capture any timing path) => column 3
set_false_path -to [get_clock *S]
# 4/ Capture clock can not launch timing path => rows 4, 5, 8 and 9
set_false_path -from [get_clock *_capture*]
# 5/ Launch clock can not capture timing path => columns 6 and 7
set_false_path -to [get_clock *_launch*]
# 6/ Disable paths between opposite edges of the acknowledge event => cells 6-9 and 7-8
set_false_path -from [get_clock RC*H_launchR] -to [get_clock RC*H_captureF]
set_false_path -from [get_clock RC*H_launchF] -to [get_clock RC*H_captureR]
# 7/ Hold LCS are not used for setup timing analysis => cells 6-8, 7-9, 6-4, 6-5, 7-4, 7-5, 3-8 and 3-9
set_false_path -setup -from [get_clock *RC*H*]    
set_false_path -setup -to [get_clock *RC*H*]    
# 8/ Setup LCS are not used for hold timing analysis => cells 3-4, 3-5, 6-4, 6-5, 7-4, 7-5, 3-8 and 3-9
set_false_path -hold -from [get_clock *RC*S*]    
set_false_path -hold -to [get_clock *RC*S*]    


#####
## Others...
#####

# propage clock to get effective timings
set_propagated_clock [all_clocks]

# Do not infer clock gating check for now...
set timing_disable_clock_gating_checks true

## This variable is required to analyze timing path between the opening of launch-latch to the closing of capture-latch.
#   If not defined, transparent latch + timing borrowing analysis is performed by the tool.
set timing_enable_through_paths true

