###############
# LCS RULES - Maximus Scheme 
###############
# Author : Grégoire Gimenez
# Date   : 2019/02/28
###############
# Rev 0.1
#   - initial release of the LCS rules
###############
#
## Register controller schematic:
######
#                                                      clk
#                                                       |
#          ,--------------------------------------------)-------------------------------------.
#          |          ,----------------------.          |                                     |
#          |      _   `-| \   ____ rstb      |          |                                     | 
#          |-----| \    |&B)--\   \  |   _   |          |            ,----------------------. |
#          |     |&I)-,-|_/    |   \ `--| \  |          |       __   `-| \   ____ rstb      | |
#          |   ,-|_/  `-| \    |    \   |gR)-+----,-|>o-'-------\ \    |&A)--\   \  |   _   | |
#    lr ---)---| __     |&C)---|OR_Z )--|_/  |    |             ||I)-,-|_/    |   \ `--| \  | |
#          |   '-\ \  ,-|_/    |    /        |    |           ,-/_/  `-| \    |    \   |gR)-+-' s
#          |     ||I)-'-| \    |   /         |    |           |        |&C)---|OR_Z )--|_/  | 
#          |  ,--/_/    |&A)--/___/     CL   |    |           |      ,-|_/    |    /        | 
#          |  |       ,-|_/                  |    |           |------'-| \    |   /         | 
#          |  |       '----------------------'    |           |        |&B)--/___/     CS   | 
#    la ---)--)---,-------------------------------'           |      ,-|_/                  |       
#          |  |   |              ,----------------------.     |      '----------------------'       
#          |  |   |          _   `-| \   ____ rstb      |     |
#          |  |---)---------| \    |&B)--\   \  |   _   |     |
#          |  |   |         |&I)-,-|_/    |   \ `--| \  |     |
#          |  |   |       ,-|_/  `-| \    |    \   |gR)-+-|>o-'----------------------------------- rr
#          |  |   |  __   |        |&C)---|OR_Z )--|_/  | 
#          |  |   '--\ \  |--------|_/    |    /        | 
#          |  |      |  )-'--------| \    |   /         | 
#          '--)------/_/           |&A)--/___/     CR   | 
#             |    or_laps       ,-|_/                  | 
#             |                  '----------------------' 
#             '----------------------------------------------------------------------------------- ra
# 
## LCS tree:
######
#              
#    Stage i-1      :      Stage i       :      Stage i+1
#                   :                    :
#     RCi_launch  <-:-- RCi (root_clock) : 
#                   :    '-> RCi_phase1 -:-> RCiS_phase2 -.                 
#                   : ,-- RCi_phase3 <---:----------------'
#                   : '------------------:-> RCi_capture
#                   :                    :
#  
## Valid paths:
######
# * setup: RCi -> RCiS_capture
# * hold:  RCi_launch -> RCi
#
## Clock interactions table:
######
#    (X->false path, H->active hold, S-> active setup)
# vfrom/to>           1   2   3   4   5
#  RCi          - 1   X   S   X   X   X
#  RCi_capture  - 2   X   X   X   X   X
#  RCi_launch   - 3   H   X   X   X   X
#  RCi_phase*   - 4   X   X   X   X   X
#  *DMC*        - 5   X   X   X   X   X
#

## Check that LCS register controller list has been previously defined
if { ! [info exist LCS_reg_ctrl_list] } {
  puts "ERROR: Could not find any register controller list"
  exit 0
}

source [file join [file dirname [info script]] functions.tcl]


#####
## First step : create event on each reg controller
#####

# Register controllers
foreach {clk ctrl_period} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl_period 0]
  set period [lindex $ctrl_period 1]
  # Create LCS root clock at $ctrl/cl_gate_R/Y."
  create_clock -name $clk -period $period [get_pin $ctrl/cl_gate_R/Y]
  # Create LCS dummy clock at $ctrl/cs_gate_R/Y."
  create_clock -name ${clk}_DMCS -period $period [get_pin $ctrl/cs_gate_R/Y]
  # Create LCS dummy clock at $ctrl/cr_gate_R/Y."
  create_clock -name ${clk}_DMCR -period $period [get_pin $ctrl/cr_gate_R/Y]
  # Propagate LCS root clock through dummy clock R (DMCR)."  
  create_generated_clock -name ${clk}_phase1 -add -combinational -master [get_clocks ${clk}] -divide_by 1 -source [get_pin ${ctrl}/or_laps/A2] [get_pin ${ctrl}/cr_gate_R/Y]
}


#####
## Second.0 step : create generated clock for phase 2 hold, capture hold, phase 1 setup and phase 2 setup
#####

# update_timing required to propagate actual root and phase clocks
update_timing

# Initialize tables
array set genclock_table_hold ""
array set genclock_table_setup ""

# get clock relations for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  # Get root clock propagating to right acknowledge pin of each register controller. Filter dummy clock.
  set genclock_table_hold($clk) [get_attribute [filter_collection [get_attribute -quiet [get_pin ${ctrl}/cl_or_I/A1] clocks] "full_name=~RC*&&full_name!~*DMC*"] full_name]
  # Get phase1 clock propagating to left request pin of each register controller.
  #set genclock_table_setup($clk) [get_attribute [filter_collection [get_attribute [get_pin ${ctrl}/cl_and_I/A1] clocks] "full_name=~*phase1&&full_name!~DMC*&&full_name!~RC$clk"] full_name] 
  set genclock_table_setup($clk) [get_attribute [filter_collection [get_attribute -quiet [get_pin ${ctrl}/cl_and_I/A1] clocks] "full_name=~*phase1"] full_name] 
}

# create generated clock for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  foreach genclk $genclock_table_hold($clk) {
    create_generated_clock -name ${genclk}_launch -add -combinational -master [get_clocks ${genclk}] -divide_by 1 -source [get_pin ${ctrl}/cl_or_I/A1] [get_pin ${ctrl}/cl_gate_R/Y]
    ## Stop hold clock propagation upstream from the current controller
    set_sense -stop_propagation -clocks ${genclk}_launch [get_pin ${ctrl}/cl_and_I/A1]
    set_sense -stop_propagation -clocks ${genclk}_launch [get_pin ${ctrl}/cl_or_I/A2]
  }
  foreach genclk $genclock_table_setup($clk) {
    create_generated_clock -name [regsub "phase1" ${genclk} "phase2"] -add -combinational -master [get_clocks ${genclk}] -divide_by 1 -source [get_pin ${ctrl}/cl_and_I/A1] [get_pin ${ctrl}/cl_gate_R/Y]
  }
}


#####
## Second.1 step : create generated clock for launch hold and phase 3 setup
#####

# update_timing required to propagate actual phase clocks
update_timing

# Initialize tables
array set genclock_table_setup ""

# get clock relations for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  # Get phase2 clock propagating to left request pin of each register controller.
  set genclock_table_setup($clk) [get_attribute [filter_collection [get_attribute -quiet [get_pin ${ctrl}/cr_and_I/A1] clocks] "full_name=~*phase2"] full_name] 
}

# create generated clock for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  foreach genclk $genclock_table_setup($clk) {
    create_generated_clock -name [regsub "phase2" ${genclk} "phase3"] -add -combinational -master [get_clocks ${genclk}] -divide_by 1 -source [get_pin ${ctrl}/cr_and_I/A1] [get_pin ${ctrl}/cr_gate_R/Y]
  }
}


#####
## Second.2 step : create generated clock for setup capture
#####

# update_timing required to propagate actual phase clocks
update_timing

# Initialize tables
array set genclock_table_setup ""

# get clock relations for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  # Get phase3 clock propagating to right acknowledge pin of each register controller.
  set genclock_table_setup($clk) [get_attribute [filter_collection [get_attribute -quiet [get_pin ${ctrl}/cl_and_I/A1] clocks] "full_name=~*phase3"] full_name] 
}

# create generated clock for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  foreach genclk $genclock_table_setup($clk) {
    create_generated_clock -name [regsub "phase3" ${genclk} "capture"] -add -combinational -master [get_clocks ${genclk}] -divide_by 1 -source [get_pin ${ctrl}/cl_and_I/A1] [get_pin ${ctrl}/cl_gate_R/Y]
    ## Stop setup capture clock propagation that to avoid loops
    set_sense -stop_propagation -clocks [regsub "phase3" ${genclk} "capture"] [get_pin ${ctrl}/cl_and_I/A1]
    set_sense -stop_propagation -clocks [regsub "phase3" ${genclk} "capture"] [get_pin ${ctrl}/cl_or_I/A2]
  }
}


#####
## Third step: define exceptions
#####

# Fully asynchronous timing
set_multicycle_path -1 -hold  -from [get_clocks *] -to [get_clocks *]
set_multicycle_path 0 -setup -from [get_clocks *] -to [get_clocks *]


## Disable inter LCS paths
set cmd "set_clock_group -asynchronous "
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  lappend cmd "-group \[trace_generated_clock -flat ${clk}\]"
}
echo [join $cmd]
eval [join $cmd]


## Disable intra LCS paths (=> see clock interactions table)
# 1/ Disable paths from/to the same root clock (a root clock can not be both launch and capture clock of the same LCS) => cell 1-1 
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set_false_path -from [get_clocks $clk] -to [get_clocks $clk]
}
# 2/ Disable timing to generated clocks that do not capture any path => columns 4 and 3
set_false_path -to [get_clocks "*_launch *phase*"]
# 3/ Disable timing from generated clocks that do not launch any path => rows 2 and 4
set_false_path -from [get_clocks "*phase* *_capture"]
# 4/ Disable setup check for clock dedicated to hold check => cells 3-1 and 3-2
set_false_path -setup -from [get_clocks -quiet "*_launch"]
# 5/ Disable hold check for clock dedicated to setup check => cells 1-2 and 3-2
set_false_path -hold -to [get_clocks -quiet "*_capture"]
# 6/ No timing should be checked for dummy clocks => row 5 and column 5
set_false_path -from [get_clocks -quiet *DMC*]
set_false_path -to [get_clocks -quiet *DMC*]


#####
## Others...
#####

# propage clock to get effective timings
set_propagated_clock [all_clocks]

# Do not infer clock gating check for now...
set timing_disable_clock_gating_checks true

