###############
# LCS RULES - Early Acknowledgement Scheme 
###############
# Author : Grégoire Gimenez
# Date   : 2019/02/28
###############
# Rev 0.1
#   - initial release of the LCS rules
###############
#
## Register controller schematic:
######
#          clk                 
#           | 
#           o inv_la
#          /_\     _                      inv_rr                 
#           |     / |-----------------------o<|------------.
#    la ----+----(&A1    ,---------------------------------)-------- ra
#           |     \_|-.  |     ,----------------------.    |
#           |         |  |     `-| \   ____ rstb      |    |
#           |         |  |       |&B)--\   \  |   _   |    |
#           |  or_TRD |  '-|>o-,-|_/    |   \ `--| \  |    |
#           |----\ \  |        `-| \    |    \   |gR)-+----'-------- rr
#           |    |  )-)-.  _     |&C)---|OR_Z )--|_/  | 
#           '-|>-/_/  | '-| \  ,-|_/    |    /        | 
#         buff_TRD*   |   &A2)-'-| \    |   /         | 
#     lr ---------|>--'--o|_/    |&A)--/___/          | 
#              buff_lr         ,-|_/                  | 
#                              '----------------------' 
#
## LCS tree:
######
#
#      Stage i-1    :       Stage i        :    Stage i+1             
#                   :                      :
#                   : ,- RCiS (root_clock) : 
#                   : '-> RCiS_phase1 -----:-> RCiS_phase2 -.                 
#                   : ,-- RCiS_phase3 <----:----------------'
#                   : '--------------------:-> RCiH_capture                
#                   :                      :                       
#                   :                      :                       
#                   :      (root clk)      :
#  ,- RCiH_phase1 <-:-------- RCiH --------:-> RCiH_capture
#  '----------------:----> RCiH_launch     :
#  
## Valid paths:
######
# * setup: RCiS -> RCiS_capture
# * hold:  RCiH_launch -> RCiH_capture
#
## Clock interactions table:
######
#    (X->false path, H->active hold, S-> active setup)
# vfrom/to>            1   2   3   4   5   6
#  RCi*_phase*   - 1   X   X   X   X   X   X   
#  RCiS_capture  - 2   X   X   X   X   X   X
#  RCiS          - 3   X   S   X   X   X   X
#  RCiH          - 4   X   X   X   X   X   X 
#  RCiH_launch   - 5   X   X   X   X   X   H
#  RCiH_capture  - 6   X   X   X   X   X   X
#

## Check that LCS register controller list has been previously defined
if { ! [info exist LCS_reg_ctrl_list] } {
  puts "ERROR: Could not find any register controller list"
  exit 0
}

source [file join [file dirname [info script]] functions.tcl]

#####
## First step : create event on each reg controller
#####

# Register controllers
foreach {clk ctrl_period} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl_period 0]
  set period [lindex $ctrl_period 1]
  # Create LCS setup root clock at $ctrl/buff_lr/Y
  create_clock -name ${clk}S -period $period [get_pin $ctrl/buff_lr/Y]
  # Create LCS hold root clock at $ctrl/gate_R/Y."
  create_clock -name ${clk}H -period $period [get_pin $ctrl/gate_R/Y]
  # Propagate LCS setup root clock through hold root clock."  
  create_generated_clock -name ${clk}S_phase1 -add -combinational -master [get_clocks ${clk}S] -divide_by 1 -source [get_pin ${ctrl}/and_A/A2] [get_pin ${ctrl}/gate_R/Y]
  # Stop hold root clock propagation that creates combinational loop through reset pulse block and and_A2 cell
  set_sense -stop_propagation -clocks ${clk}S [get_pin ${ctrl}/and_A2/A1]
}


#####
## Second.0 step : create generated clock for phase 1 hold, capture hold and phase 2 setup
#####

# update_timing required to propagate actual root and phase clocks
update_timing

# Initialize tables
array set genclock_table_hold_capture ""
array set genclock_table_hold ""
array set genclock_table_setup ""

# get clock relations for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  # Get hold root clock propagating to left request pin of each register controller.
  set genclock_table_hold_capture($clk) [get_attribute [filter_collection [get_attribute -quiet [get_pin ${ctrl}/buff_lr/A] clocks] "full_name=~*H"] full_name] 
  # Get hold root clock propagating to right acknowledge pin of each register controller.
  set genclock_table_hold($clk) [get_attribute [filter_collection [get_attribute -quiet [get_pin ${ctrl}/inv_ra/Y] clocks] "full_name=~*H"] full_name] 
  # Get setup phase1 clock propagating to left request pin of each register controller.
  set genclock_table_setup($clk) [get_attribute [filter_collection [get_attribute -quiet [get_pin ${ctrl}/buff_lr/A] clocks] "full_name=~*S_phase1"] full_name] 
}

# create generated clock for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  foreach genclk $genclock_table_hold_capture($clk) {
    create_generated_clock -name ${genclk}_capture -add -combinational -master [get_clocks ${genclk}] -divide_by 1 -source [get_pin ${ctrl}/buff_lr/A] [get_pin ${ctrl}/buff_lr/Y]
  }
  foreach genclk $genclock_table_hold($clk) {
    create_generated_clock -name ${genclk}_phase1 -add -combinational -master [get_clocks ${genclk}] -divide_by 1 -source [get_pin ${ctrl}/inv_ra/Y] [get_pin ${ctrl}/gate_R/Y]
    ## Stop hold clock propagation upstream from the current controller
    #set_sense -stop_propagation -clocks RC${clk}_${genclk}_phase1 [get_pin ${ctrl}/and_rr/A1]
  }
  foreach genclk $genclock_table_setup($clk) {
    create_generated_clock -name [regsub "phase1" ${genclk} "phase2"] -add -combinational -master [get_clocks ${genclk}] -divide_by 1 -source [get_pin ${ctrl}/buff_lr/A] [get_pin ${ctrl}/buff_lr/Y]
    ## Stop setup clock propagation upstream from the current controller
    #set_clock_sense -stop_propagation -clock RC${clk}_${genclk}_phase2 [get_pin ${ctrl}/and_rr/A1]
  }
}


#####
## Second.1 step : create generated clock for launch hold and phase 3 setup
#####

# update_timing required to propagate actual phase clocks
update_timing

# Initialize tables
array set genclock_table_hold ""
array set genclock_table_setup ""

# get clock relations for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  # Get hold phase1 clock propagating to left request pin of each register controller.
  set genclock_table_hold($clk) [get_attribute [filter_collection [get_attribute -quiet [get_pin ${ctrl}/buff_lr/A] clocks] "full_name=~*H_phase1"] full_name] 
  # Get setup phase2 clock propagating to right acknowledge pin of each register controller.
  set genclock_table_setup($clk) [get_attribute [filter_collection [get_attribute -quiet [get_pin ${ctrl}/inv_ra/Y] clocks] "full_name=~*S_phase2"] full_name] 
}

# create generated clock for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  foreach genclk $genclock_table_setup($clk) {
    create_generated_clock -name [regsub "phase2" ${genclk} "phase3"] -add -combinational -master [get_clocks ${genclk}] -divide_by 1 -source [get_pin ${ctrl}/inv_ra/Y] [get_pin ${ctrl}/gate_R/Y]
  }
  foreach genclk $genclock_table_hold($clk) {
    create_generated_clock -name [regsub "phase1" ${genclk} "launch"] -add -combinational -master [get_clocks ${genclk}] -divide_by 1 -source [get_pin ${ctrl}/buff_lr/A] [get_pin ${ctrl}/buff_lr/Y]
  }
}


#####
## Second.2 step : create generated clock for setup capture
#####

# update_timing required to propagate actual phase clocks
update_timing

# Initialize table
array set genclock_table_setup ""

# get clock relations for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  # Get setup phase3 clock propagating to left request pin of each register controller.
  set genclock_table_setup($clk) [get_attribute [filter_collection [get_attribute -quiet [get_pin ${ctrl}/buff_lr/A] clocks] "full_name=~*S_phase3"] full_name] 
}

# create generated clock for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  foreach genclk $genclock_table_setup($clk) {
    create_generated_clock -name [regsub "phase3" ${genclk} "capture"] -add -combinational -master [get_clocks ${genclk}] -divide_by 1 -source [get_pin ${ctrl}/buff_lr/A] [get_pin ${ctrl}/buff_lr/Y]
  }
}


#####
## Third step: define exceptions
#####

# Fully asynchronous timing
set_multicycle_path -1 -hold  -from [get_clocks *] -to [get_clocks *]
set_multicycle_path 0 -setup -from [get_clocks *] -to [get_clocks *]


## Disable inter LCS paths
set cmd "set_clock_group -asynchronous "
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  lappend cmd "-group \[trace_generated_clock -flat ${clk}S\] -group \[trace_generated_clock -flat ${clk}H\]"
}
echo [join $cmd]
eval [join $cmd]


## Disable intra LCS paths (=> see clock interactions table)
# 1/ Disable timing to generated clocks that do not capture any path => columns 1 and 5
set_false_path -to [get_clocks "*_launch *phase*"]
# 2/ Disable timing from generated clocks that do not launch any path => rows 1, 2 and 6
set_false_path -from [get_clocks "*phase* *_capture"]
# 3/ Disable timing path from and to hold root clock (do not directly capture nor launch any path) => row 4 and column 4
set_false_path -from [get_clocks -quiet "*H"]
set_false_path -to [get_clocks -quiet "*H"]
# 4/ Disable timing path to setup root clock (do not directly capture any path) => column 3
set_false_path -to [get_clocks -quiet "*S"]
# 5/ Disable setup check for clock dedicated to hold check => cells 3-6, 5-6 and 5-2
set_false_path -setup -from [get_clocks -quiet "*H*"]
set_false_path -setup -to [get_clocks -quiet "*H*"]
# 6/ Disable hold check for clock dedicated to setup check => cells 3-2, 3-6 and 5-2
set_false_path -hold -from [get_clocks -quiet "*S*"]
set_false_path -hold -to [get_clocks -quiet "*S*"]


#####
## Others...
#####

# propage clock to get effective timings
set_propagated_clock [all_clocks]

# Do not infer clock gating check for now...
set timing_disable_clock_gating_checks true

