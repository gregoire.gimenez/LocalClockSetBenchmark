###############
# LCS RULES - Click Scheme 
###############
# Author : Grégoire Gimenez
# Date   : 2019/02/28
###############
# Rev 0.1
#   - initial release of the LCS rules
###############
#
## Register controller schematic:
######
#                        clk
#                         |
#               _         |
#    lr --.---o| \ ___    |---.
#    la --|-,--|&1)\  \   |  _|_   
#       ,-)-)--|_/  |OR\__| | v |
#       | `-)--| \  |  /    |ffc|
#       |   |-o|&2)/__/  ,-o|___|--.
#       |---)-o|_/       |         |
#       |   `------------'---------'----- rr
#       `-------------------------------- ra
#  
#
## LCS tree:
######
#
#    Stage i-1      :              Stage i             :      Stage i+1
#                   :                                  :
#                   :                   ,-> RCi_pulse  :
#                   :                   |              :
#                   :     RCi  -> RCi_phase1 ----------:---> RCi_launchR 
#                   : (root clock)      |              : `-> RCi_launchF
#  RCi_captureR <---:-------------------'              :           
#  RCi_captureF <-' :                                  :
#                   :                                  :
#
## Valid paths:
######
# * setup: RCi -> RCi_captureR (propagation of a rising edge on the request path)
#          RCi -> RCi_captureF (propagation of a falling edge on the request path)
# * hold:  RCi_launchR -> RCi (propagation of a rising edge on the acknowledge path)
#          RCi_launchF -> RCi (propagation of a falling edge on the acknowledge path)
#
## Clock interactions table:
######
#    (X->false path, H->active hold, S-> active setup)
# vfrom/to>            1   2   3   4   5   6   7
#  RCi_phase1    - 1   X   X   X   X   X   X   X
#  RCi_captureR  - 2   X   X   X   X   X   X   X
#  RCi_captureF  - 3   X   X   X   X   X   X   X
#  RCi           - 4   X   S   S   X   X   X   X
#  RCi_launchR   - 5   X   X   X   H   X   X   X
#  RCi_launchF   - 6   X   X   X   H   X   X   X
#  RCi_pulse     - 7   X   X   X   X   X   X   X
#  

## Env variable used to enable pulse clock creation
if { [info exist env(define_pulse_clock)] } {
  set define_pulse_clock true
} else {
  set define_pulse_clock false
}

## Check that LCS register controller list has been previously defined
if { ! [info exist LCS_reg_ctrl_list] } {
  puts "ERROR: Could not find any register controller list"
  exit 0
}

source [file join [file dirname [info script]] functions.tcl]

#####
## First step : create event on each reg controller
#####

## Creating root clock and propagating it in each controller. For click template, setup and hold root clock are the same
foreach {clk ctrl_period} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl_period 0]
  set period [lindex $ctrl_period 1]
  ## Create root clock for both setup and hold at the output of the phase converter (= output pin Y of the OR gate)
  create_clock -name ${clk} -period $period [get_pin ${ctrl}/ctl_or/Y]
  ## Propagate root clock through phase flip-flop ${ctrl}/ffc (CLK->Q)
  create_generated_clock -name ${clk}_phase1 -edges {1 1 3} -source [get_pin ${ctrl}/ffc/CLK] [get_pin ${ctrl}/ffc/Q] -master ${clk} -add
  if { $define_pulse_clock } {
    ## Propagate pulse LCS back through the phase converter block
    create_generated_clock -name ${clk}_pulse -invert -edges {1 1 3} -source [get_pin ${ctrl}/ffc/Q] [get_pin ${ctrl}/ctl_or/Y] -master ${clk}_phase1 -add
  }
}

#####
## Second step: create generated clocks for launch (hold) and capture (setup)
#####

# update_timing required to propagate actual clocks
update_timing

# Initialize tables
array set genclock_table_hold ""
array set genclock_table_setup ""

# get clock relations for each register controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  # Get hold root clock propagating to right acknowledge pin of each register controller.
  set genclock_table_hold($clk) [get_attribute [get_clock -quiet [get_attribute -quiet [get_pin ${ctrl}/ctl_and1/A3] clocks] -filter "full_name=~*phase1*"] full_name]
  # Get setup root clock propagating to left request pin of each register controller.
  set genclock_table_setup($clk) [get_attribute [get_clock -quiet [get_attribute -quiet [get_pin ${ctrl}/ctl_and2/A1] clocks] -filter "full_name=~*phase1"] full_name]
}

# create generated clock for each register controller
## Restrictive description of the event paths using clock sense (stop propagation) => saves 4 clock definitions per controller
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set ctrl [lindex $ctrl 0]
  foreach genclk $genclock_table_hold($clk) {
    # Propagate rising hold LCS through phase converter block (${ctrl}/ctl_and1/A3->${ctrl}/ctl_or/Y)"
    create_generated_clock -name [regsub "phase1" ${genclk} "launchR"] -edges {1 2 3} -source [get_pin ${ctrl}/ctl_and1/A3] [get_pin ${ctrl}/ctl_or/Y] -master ${genclk} -add
    set_sense -clocks [regsub "phase1" ${genclk} "launchR"] -stop_propagation ${ctrl}/ctl_and2/A3
    # Propagate rising setup LCS through phase converter block (${ctrl}/ctl_and2/A3->${ctrl}/ctl_or/Y)"
    create_generated_clock -name [regsub "phase1" ${genclk} "launchF"] -edges {1 2 3} -source [get_pin ${ctrl}/ctl_and2/A3] [get_pin ${ctrl}/ctl_or/Y] -master ${genclk} -add
    set_sense -clocks [regsub "phase1" ${genclk} "launchF"] -stop_propagation ${ctrl}/ctl_and1/A3
  }
  foreach genclk $genclock_table_setup($clk) {
    # Propagate rising setup LCS through phase converter block (${ctrl}/ctl_and2/A1->${ctrl}/ctl_or/Y)"
    create_generated_clock -name [regsub "phase1" ${genclk} "captureR"] -edges {1 2 3} -source [get_pin ${ctrl}/ctl_and2/A1] [get_pin ${ctrl}/ctl_or/Y] -master ${genclk} -add
    set_sense -clocks [regsub "phase1" ${genclk} "captureR"] -stop_propagation ${ctrl}/ctl_and1/A1
    # Propagate falling setup LCS through phase converter block (${ctrl}/ctl_and1/A1->${ctrl}/ctl_or/Y)"
    create_generated_clock -name [regsub "phase1" ${genclk} "captureF"] -edges {1 2 3} -source [get_pin ${ctrl}/ctl_and1/A1] [get_pin ${ctrl}/ctl_or/Y] -master ${genclk} -add
    set_sense -clocks [regsub "phase1" ${genclk} "captureF"] -stop_propagation ${ctrl}/ctl_and2/A1
  }
}


#####
## Third step: define exceptions
#####

# Fully asynchronous timing
set_multicycle_path -1 -hold  -from [get_clocks *] -to [get_clocks *]
set_multicycle_path 0 -setup -from [get_clocks *] -to [get_clocks *]


## Disable inter LCS paths
set group_cmd "set_clock_groups -asynchronous"
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  lappend group_cmd "-group \[get_clock -quiet \"${clk} ${clk}_phase1 ${clk}_capture* ${clk}_launch*\"\]"
}
echo [join $group_cmd]
eval [join $group_cmd]


## Disable intra LCS paths (=> see clock interactions table)
# 1/ Disable paths from/to event propagation clocks (phase*) => row 1 and column 1
set_false_path -from [get_clock *phase*]
set_false_path -to [get_clock *phase*]
# 2/ Capture clock can not launch timing path => rows 2 and 3
set_false_path -from [get_clock *_capture*]
# 3/ Launch clock can not capture timing path => columns 5 and 6
set_false_path -to [get_clock *_launch*]
# 4/ Hold LCS are not used for setup timing analysis => cells 5-4 and 6-4 and box (5-2,6-3)
set_false_path -setup -from [get_clock *_launch*]    
# 5/ Setup LCS are not used for hold timing analysis => cells 4-2 and 4-3 and box (5-2,6-3)
set_false_path -hold -to [get_clock *_capture*]    
# 6/ Disable path between the same root clock => cell 4-4
foreach {clk ctrl} [array get LCS_reg_ctrl_list] {
  set_false_path -from [get_clock $clk] -to [get_clock $clk]
}
# 7/ Pulse LCS are not used for setup neither for hold timing analysis => row 7 and column 7
if { $define_pulse_clock } {
  set_false_path -from [get_clock *pulse*]    
  set_false_path -to [get_clock *pulse*]    
}


#####
## Others...
#####

# propage clock to get effective timings
set_propagated_clock [all_clocks]

# Do not infer clock gating check for now...
set timing_disable_clock_gating_checks true

