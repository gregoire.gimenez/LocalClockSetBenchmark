###############
# LCS RULES - SYNCHRONOUS 
###############
# Author : Grégoire Gimenez
# Date   : 2019/02/28
###############
# Rev 0.1
#   - initial release of the LCS rules
###############

## Main clock
create_clock -name clk -period 1 [get_port lr]

## Propagate clock to be in the same state than async template
set_propagated_clock [get_clock clk]
