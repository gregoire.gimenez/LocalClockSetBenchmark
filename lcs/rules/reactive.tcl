###############
# LCS RULES - Reactive Circuit Scheme 
###############
# Author : Grégoire Gimenez
# Date   : 2019/02/28
###############
# Rev 0.1
#   - initial release of the LCS rules
###############
#
## Register controller schematic:
######
# 
#    ,-------------------------.
#    |                         |
#    |         (dmy,edge*_tmp) |
#    |                  |      |
#    |                  v  _   |
#    `-|>-|>-|>-|>-|>-|>--| \  |               1___2   3___
#      B0 B1 B2 B3 B4 B5  |&Ro----- osc_out  __|   |___|
#                   ,---- |_/ ^
#                   |         |
#                 rstb    (edge1,2,3)
# 
## LCS tree:
######
#   edge1 (root clock) -> edge1_tmp -> edge2 (inv) -> edge2_tmp -> edge3 (inv)
#
## Valid paths:
######
# * setup: edge1 -> edge3
# * hold: edge1 -> edge1
#
# Note: edge3 -> edge3 is also a valid hold path but it is redundant with edge1 -> edge1
#
## Clock interactions table:
######
#    (X->false path, H->active hold, S-> active setup)
# vfrom/to>    1   2   3
#  edge1 - 1   H   X   S 
#  edge2 - 2   X   X   X
#  edge3 - 3   X   X   X
#


#####
## First step : create root and dummy clocks
#####

## Create root clock edge1 (rising edge) at the output of the ring oscillator (RO)
create_clock -name edge1 -period 0.0001 [get_pin gate_R/Y]
## Create a dummy clock on a stage of the RO (avoid clock propagation issue due to root clock loop on itself)
create_clock -name dmy -period 0.0001 [get_pin osc_5/Y]


#####
## Second step: create generated clocks
#####

## Create propagation clock for edge1 to cross dummy clock source point
create_generated_clock -name edge1_tmp -combinational -divide_by 1 -master edge1     -add -source [get_pin osc_5/A]   [get_pin osc_5/Y]
## Create propagation clock edge2 (falling edge) (crossing edge1 clock source point)
create_generated_clock -name edge2     -combinational -divide_by 1 -invert -master edge1_tmp -add -source [get_pin gate_R/A1] [get_pin gate_R/Y]
## Create propagation clock for edge2 to cross dummy clock source point
create_generated_clock -name edge2_tmp -combinational -divide_by 1 -master edge2     -add -source [get_pin osc_5/A]   [get_pin osc_5/Y]
## Create propagation clock edge3 (rising edge) (crossing edge1 clock source point)
create_generated_clock -name edge3     -combinational -divide_by 1 -invert -master edge2_tmp -add -source [get_pin gate_R/A1] [get_pin gate_R/Y]


#####
## Third step: define exceptions
#####

## Fully asynchronous timing
set_multicycle_path -1 -hold  -from [get_clocks *] -to [get_clocks *]
set_multicycle_path 0 -setup -from [get_clocks *] -to [get_clocks *]

## Disable intra LCS paths (=> see clock interactions table)
# 1/ Disable paths launched by edge2 and edge3 (only edge1 is used) => row 2 and 3
set_false_path -from [get_clocks "*edge3*"]
set_false_path -from [get_clocks "*edge2*"]
# 2/ Disable paths captured by edge2 => column 2
set_false_path -to [get_clocks "*edge2*"]
# 3/ Disable setup paths captured by edge1 => cell 1-1
set_false_path -setup -to [get_clocks "*edge1*"]
# 4/ Disable hold path captured by edge3 => cell 1-3
set_false_path -hold -to [get_clocks "*edge3*"]


#####
## Others...
#####
# propage clock to get effective timings
set_propagated_clock [get_clock *]

