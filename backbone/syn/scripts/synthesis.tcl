source -echo -verbose ./scripts/setup.tcl

## Some synthesis options
set_app_var hdlin_infer_multibit default_all

## In case equ.checking with Formality is planned
set_svf ${RESULTS}/${DESIGN}.mapped.svf

define_design_lib WORK -path ./WORK

## Analyze design
analyze -autoread -rebuild -recursive -top ${DESIGN} -output_script ${RESULTS}/${DESIGN}.autoread_rtl.tcl ${RTL_SRC_FILES}

## Elaborate design
elaborate ${DESIGN} -param "l=>$env(use_latch),n=>$env(pipe_size)"

## write elaborated design
write -hierarchy -format ddc -output ${RESULTS}/${DESIGN}.elaborated.ddc

## Some synthesis options
set_multibit_options -mode timing_driven
remove_path_group [get_attribute [get_path_groups] name]
set_fix_multiple_port_nets -all -buffer_constants

## Do not modify cells in the template (they have been manually mapped on the technonlogy)
set_dont_touch [get_cell -hier -filter "ref_name=~*_RVT"] true

## Do not bother with the reset tree
set_ideal_network reset
set_ideal_transition 0.3 reset

## Check every things is ok
check_design -summary
check_design > ${REPORTS}/check_design.rpt

## Compile
compile_ultra -no_autoungroup
## Re-compile
compile_ultra -incremental -no_autoungroup
## Optimize
optimize_netlist -area
## Some cleaning
change_names -rules verilog -hierarchy

## Write output files
write -format verilog -hierarchy -output ${RESULTS}/${DESIGN}.mapped.v
#write -format ddc     -hierarchy -output ${RESULTS}/${DESIGN}.mapped.ddc

set_svf -off

exit

