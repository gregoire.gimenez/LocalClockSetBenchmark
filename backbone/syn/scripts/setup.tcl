## Check env variable existence
if { [info exists env(template)] } {
  set template $env(template)
} else {
  puts "ERROR : please define the 'template' env variable"
  exit 0 
}

## Suppress some flooding messages
set_app_var sh_new_variable_message false

## Specify the name of the top module
set DESIGN "pipe"

## define technology
set_app_var search_path ". ../../libraries/saed32nm_rvt/db/ ../sdc/ $search_path"
set_app_var target_library saed32rvt_tt1p05v25c.db
set_app_var synthetic_library dw_foundation.sldb
set_app_var link_library "* $target_library $synthetic_library"

## Some tool's parameters
set_host_options -max_cores 4
set_app_var alib_library_analysis_path .

## Define RTL input files ( 
set RTL_SRC_FILES  "../../rtl/delay.vhd
                    ../../rtl/delay_chain.vhd
                    ../../rtl/mem.vhd
                    ../../rtl/scs/sc_$template.vhd"
if { $template == "sync" } {
  set RTL_SRC_FILES  [join "$RTL_SRC_FILES
                            ../../rtl/pipe5sync.vhd
                            ../../rtl/pipe.vhd"]
} elseif { $template == "reactive" } {
  set RTL_SRC_FILES  [join "$RTL_SRC_FILES
                            ../../rtl/pipe5sync.vhd
                            ../../rtl/pipereactive.vhd"]
} else {
  set RTL_SRC_FILES  [join "$RTL_SRC_FILES
                            ../../rtl/pipe5.vhd
                            ../../rtl/pipe.vhd"]
}

## Define reports and results directories
set REPORTS "reports"
set RESULTS "results"

## Create them if required
file mkdir ${REPORTS}
file mkdir ${RESULTS}

