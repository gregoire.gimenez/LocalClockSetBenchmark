## Check env variable existence
if { ! [info exists env(template)] } {
  puts "ERROR : please define the 'template' env variable"
  exit 0 
}
if { ! [info exists env(DESIGN)] } {
  puts "ERROR : please define the 'DESIGN' env variable"
  exit 0 
}

source scripts/proc_time.tcl
proc_time START -log_prefix REPORTS/runtime -reset
 
## Load design
read_verilog ../syn/results/$env(DESIGN).mapped.v > ./REPORTS/load_design.log
current_design $env(DESIGN) >> ./REPORTS/load_design.log
link -verbose >> ./REPORTS/load_design.log
proc_time LOAD_DESIGN -log_prefix REPORTS/runtime

## 0-RC mode (no parasitics)
suppress_message UITE-450
set_load 0 [get_nets -hier] 
set_ideal_network -no_propagate [get_net -hier * -filter "total_capacitance_max>0.5"]
proc_time DEFINE_0RC -log_prefix REPORTS/runtime
unsuppress_message UITE-450

## Load LCS structure
source ../../lcs/$env(DESIGN).tcl >> ./REPORTS/load_design.log
## Load LCS rules
source ../../lcs/rules/$env(template).tcl >> ./REPORTS/load_design.log
proc_time GEN_LCS -log_prefix REPORTS/runtime

## Perform timing analysis
update_timing -full > ./REPORTS/update_timing.log
proc_time UPDATE_TIMING -log_prefix REPORTS/runtime

## Some reports
check_timing -include {clock_crossing signal_level} -verbose > ./REPORTS/check_timing.log
report_clock -nosplit > ./REPORTS/clock.log
report_port -input -output -nosplit -verbose > ./REPORTS/port.log
report_exceptions > ./REPORTS/exceptions.log
report_exceptions -ignored >> ./REPORTS/exceptions.log
report_analysis_coverage -status_detail violated -nosplit > ./REPORTS/violated.log
report_analysis_coverage -status_detail untested -nosplit > ./REPORTS/untested.log
report_disable_timing -nosplit > ./REPORTS/disabled.log
report_constraint -all_violators -nosplit > ./REPORTS/constraints.log
echo "nb_clocks: [sizeof_collection [get_clocks *]]"                                                        > ./REPORTS/design_stats.log
echo "nb_instances: [sizeof_collection [get_cell * -hier -filter "is_hierarchical==false"]]"               >> ./REPORTS/design_stats.log
echo "nb_sequential: [sizeof_collection [get_cell -hier -filter "is_hierarchical==false&&is_sequential"]]" >> ./REPORTS/design_stats.log
echo "nb_buf: [sizeof_collection [get_cell -hier -filter "is_hierarchical==false&&number_of_pins==2"]]"    >> ./REPORTS/design_stats.log
proc_time REPORTS -log_prefix REPORTS/runtime

## Save session for latter use
save_session REPORTS/SAVED_SESSION
proc_time SAVE_SESSION -log_prefix REPORTS/runtime

proc_time END -log_prefix REPORTS/runtime
exit

