## proc_time procedure is a synopsys proprietary code available here:
#    https://solvnet.synopsys.com/retrieve/customer/script/attached_files/035777/proc_time.tcl
#  For those who do not have access to this script a very basic version is proposed here which 
#  simply prints a tag and a timestamp (in nanosecond) in a logfile.

proc proc_time {args} {

  set tag [lindex $args 0]
  set logfile [lindex $args 2]
  set reset 0
  if { [lindex $args 3] == "-reset" } {
    set reset 1
  }

  if { $reset } {
    echo "$tag - [exec date +%s%N] ns" > $logfile.log
  } else {
    echo "$tag - [exec date +%s%N] ns" >> $logfile.log
  }

}
